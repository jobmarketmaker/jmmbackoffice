﻿using BOWebForms.Classes;
using System;
using System.Data;

namespace BOWebForms.Tools
{
    public partial class ResetEmployerContactPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            var searchTerm = txtCandidate.Text.Trim();

            if (searchTerm == "")
                return;

            JMMDatabase jd = new JMMDatabase();
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    jd = JMMDatabase.DEV;
                    break;
                case "s":
                    jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    jd = JMMDatabase.PRODUCTION;
                    break;
            }

            Utilities u = new Utilities();
            DataTable dt = u.SearchForEmployerContacts(searchTerm, jd);

            grdEmployerContacts.DataSource = dt;
            grdEmployerContacts.DataBind();

            pnl3.Visible = true;
        }

        protected void btn3_Click(object sender, EventArgs e)
        {
            lblEmpContactId.Text = grdEmployerContacts.SelectedRow.Cells[1].Text;
            pnl4.Visible = true;
        }

        protected void btn4_Click(object sender, EventArgs e)
        {
            JMMDatabase jd = new JMMDatabase();
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    jd = JMMDatabase.DEV;
                    break;
                case "s":
                    jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    jd = JMMDatabase.PRODUCTION;
                    break;
            }

            Utilities u = new Utilities();
            try
            {
                u.UpdateEmployerContactPassword(int.Parse(lblEmpContactId.Text), txtNewPassword.Text.Trim(), jd);
                lblFinalMessage.Text = "Employer Contact password has been reset";
            }
            catch (Exception ex)
            {
                lblFinalMessage.Text = "There was a problem resetting the Employer Contact's password; error details as follows: " + ex.Message;
            }

            pnl5.Visible = true;
        }

        protected void btn5_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResetEmployerContactPassword");
        }
    }
}