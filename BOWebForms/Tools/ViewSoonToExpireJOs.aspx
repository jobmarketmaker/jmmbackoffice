﻿<%@ Page Title="View Soon-to-Expire Job Openings" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewSoonToExpireJOs.aspx.cs" Inherits="BOWebForms.Tools.ViewSoonToExpireJOs" %>
<%@ Register Src="~/Controls/DatabaseEnvironment.ascx" TagPrefix="uc1" TagName="DatabaseEnvironment" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>View Soon-to-Expire Job Openings</h1>

    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

    <asp:Panel ID="pnl1" runat="server">

        <uc1:DatabaseEnvironment runat="server" id="DatabaseEnvironment" />

    </asp:Panel>


    <asp:Panel ID="pnl2" runat="server" Visible="false">

        <hr />
        <h3>Number of days to look ahead</h3>
        <asp:TextBox ID="txtNumberOfDays" runat="server" MaxLength="2" Width="25" Text="9" />
        <br /><br />
        <asp:Button ID="btn2" runat="server" Text="Next >>" OnClick="btn2_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl3" runat="server" Visible="false">

        <hr />
        <h3>Here is your report</h3>
        <asp:Button ID="btnToCSV" runat="server" Text="That is hideous - just get me the data in CSV format!" OnClick="btnToCSV_Click" />
        <br /><br />

        <div style="height: 250px; overflow-y:scroll;">
            <asp:GridView ID="grdJobOpenings" runat="server" Font-Size="Smaller"></asp:GridView>
        </div>

    </asp:Panel>

    <script type="text/javascript">

        //$(document).ready(function () {
        //    $(document).keypress(function (event) {
        //        if (event.which == 13) {
        //            event.preventDefault();
        //            if (document.getElementById('MainContent_btn2')) {
        //                $("#MainContent_btn2").click();
        //            }
        //        }
        //    });
        //});

    </script>

</asp:Content>
