﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BOWebForms.Tools
{
    public partial class JobOpeningInternalAudit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DatabaseEnvironment.DatabaseSelected += DatabaseEnvironment_DatabaseSelected;
        }

        private void DatabaseEnvironment_DatabaseSelected(object sender, EventArgs e)
        {
            Utilities u = new Utilities();
            try
            {
                DataTable dt = u.JobOpeningInternalAudit(DatabaseEnvironment.jd);
                grdOutput.DataSource = dt;
                grdOutput.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = "There was a problem retrieving the report; error details as follows: " + ex.Message;
            }

            pnl3.Visible = true;
        }

        //protected void btn2_Click(object sender, EventArgs e)
        //{
        //    Utilities u = new Utilities();
        //    try
        //    {
        //        DataTable dt = u.ViewSoonToExpireJobOpenings(int.Parse(txtNumberOfDays.Text), DatabaseEnvironment.jd);
        //        grdJobOpenings.DataSource = dt;
        //        grdJobOpenings.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblError.Text = "There was a problem retrieving the report; error details as follows: " + ex.Message;
        //    }

        //    pnl3.Visible = true;
        //}

        protected void btnToCSV_Click(object sender, EventArgs e)
        {
            Utilities u = new Utilities();
            DataTable dt = u.JobOpeningInternalAudit(DatabaseEnvironment.jd);

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                  Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                  string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }


            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=jobopeninginternalaudit_" + System.DateTime.Now.ToString("yyyyMMdd") + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}