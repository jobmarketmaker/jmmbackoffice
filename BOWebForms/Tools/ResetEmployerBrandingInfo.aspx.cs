﻿using BOWebForms.Classes;
using System;
using System.Data;

namespace BOWebForms.Tools
{
    public partial class ResetEmployerBrandingInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DatabaseEnvironment.DatabaseSelected += DatabaseEnvironment_DatabaseSelected;
        }

        private void DatabaseEnvironment_DatabaseSelected(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            var searchTerm = txtEmployer.Text.Trim();

            if (searchTerm == "")
                return;

            Utilities u = new Utilities();
            try
            {
                DataTable dt = u.SearchForEmployers(searchTerm, DatabaseEnvironment.jd);
                grdEmployers.DataSource = dt;
                grdEmployers.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = "There was a problem retrieving the report; error details as follows: " + ex.Message;
            }

            pnl3.Visible = true;
        }

        protected void btn3_Click(object sender, EventArgs e)
        {
            if (grdEmployers.SelectedRow == null)
                return;

            lblEmployerId.Text = grdEmployers.SelectedRow.Cells[1].Text;
            chkBranding.Checked = ((System.Web.UI.WebControls.CheckBox)grdEmployers.SelectedRow.Cells[3].Controls[0]).Checked;
            txtSubdomainPrefix.Text = grdEmployers.SelectedRow.Cells[4].Text;
            pnl4.Visible = true;
        }

        protected void btn4_Click(object sender, EventArgs e)
        {
            Utilities u = new Utilities();
            try
            {
                u.UpdateEmployerBrandingInfo(int.Parse(lblEmployerId.Text), chkBranding.Checked, txtSubdomainPrefix.Text.ToLower().Trim(), DatabaseEnvironment.jd);
                lblFinalMessage.Text = "Employer branding information has been changed";
            }
            catch (Exception ex)
            {
                lblFinalMessage.Text = "There was a problem channging the Employer's branding information; error details as follows: " + ex.Message;
            }

            pnl5.Visible = true;
        }

        protected void btn5_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResetEmployerBrandingInfo");
        }
    }
}