﻿using BOWebForms.Classes;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

namespace BOWebForms.Tools
{
    public partial class CheckTKCurrency : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DatabaseEnvironment.DatabaseSelected += DatabaseEnvironment_DatabaseSelected;
            TKEnvironment.TKSelected += TKEnvironment_TKSelected;
        }

        private void DatabaseEnvironment_DatabaseSelected(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        private void TKEnvironment_TKSelected(object sender, EventArgs e)
        {
            BindGrid();
            pnl3.Visible = true;
        }

        private void BindGrid()
        {
            string tkURL = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKMatchStagingURL"].ToString() : ConfigurationManager.AppSettings["TKMatchProdURL"].ToString();
            string tkData = "";

            bool matchAgain = true;
            int ro = 0;

            while (matchAgain)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(tkURL);
                request.Method = "POST";
                request.ContentType = "text/xml";
                XmlDocument soapEnvelopXml = new XmlDocument();
                soapEnvelopXml = CreateDummyMatchSoapEnvelope(ro);
                soapEnvelopXml.Save(request.GetRequestStream());

                string temp = "";
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    temp = reader.ReadToEnd();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                }

                XmlDocument returnDoc = new XmlDocument();
                returnDoc.LoadXml(temp);

                if (ro == 0)
                {
                    int recordCount = 0;
                    XmlNodeList xnl = returnDoc.GetElementsByTagName("matchSize");
                    if (xnl.Count == 0)
                        recordCount = 0;
                    else
                        recordCount = int.Parse(xnl.Item(0).InnerText);
                    //lblNumberOfJobOpenings.Text = recordCount.ToString();
                }

                XmlNodeList resultItemsNodes = returnDoc.GetElementsByTagName("resultItems");
                if (resultItemsNodes.Count == 0)
                    matchAgain = false;
                else
                {
                    ro += 20;
                    foreach (XmlNode rin in resultItemsNodes)
                    {
                        var fields = rin["fields"];
                        var item = fields["item"];
                        var job_opening_id = item.InnerText;
                        tkData += job_opening_id.ToString() + ",";
                    }
                }
            }
            tkData = tkData.TrimEnd(',');


            Utilities u = new Utilities();
            try
            {
                DataTable dt = u.ViewTKStatus(tkData, DatabaseEnvironment.jd);
                grdTKStatus.DataSource = dt;
                grdTKStatus.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = "There was a problem retrieving the report; error details as follows: " + ex.Message;
            }
        }

        private XmlDocument CreateDummyMatchSoapEnvelope(int resultOffset)
        {
            string tkEnvelope = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKMatchStagingEnvelope"].ToString() : ConfigurationManager.AppSettings["TKMatchProdEnvelope"].ToString();

            XmlDocument soapEnvelop = new XmlDocument();
            string decoded = HttpUtility.HtmlDecode(tkEnvelope);
            decoded = decoded.Replace("<resultOffset></resultOffset>", "<resultOffset>" + resultOffset.ToString() + "</resultOffset>");

            soapEnvelop.LoadXml(decoded);

            return soapEnvelop;
        }


        private XmlDocument CreateVacancySoapEnvelope(string EncodedXML, int Job_Opening_Id)
        {
            string tkEnvelope = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKVacancyStagingEnvelope"].ToString() : ConfigurationManager.AppSettings["TKVacancyProdEnvelope"].ToString();

            XmlDocument soapEnvelop = new XmlDocument();
            string decoded = HttpUtility.HtmlDecode(tkEnvelope);
            decoded = decoded.Replace("<fileName></fileName>", "<fileName>" + Job_Opening_Id.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmtt") + ".xml</fileName>").Replace("<fileContent></fileContent>", "<fileContent>" + EncodedXML + "</fileContent>");

            soapEnvelop.LoadXml(decoded);

            return soapEnvelop;
        }


        private XmlDocument CreateDeleteSoapEnvelope(int Job_Opening_Id)
        {
            string tkEnvelope = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKDeleteDocumentStagingEnvelope"].ToString() : ConfigurationManager.AppSettings["TKDeleteDocumentProdEnvelope"].ToString();

            XmlDocument soapEnvelop = new XmlDocument();
            string decoded = HttpUtility.HtmlDecode(tkEnvelope);
            decoded = decoded.Replace("<externalid></externalid>", "<externalid>" + Job_Opening_Id.ToString() + "</externalid>");

            soapEnvelop.LoadXml(decoded);

            return soapEnvelop;
        }

        protected void btnAct_Click(object sender, EventArgs e)
        {
            int JOID = 0;
            string actionToTake = "";
            Utilities u = new Utilities();

            try
            {
                foreach (GridViewRow gvr in grdTKStatus.Rows)
                {
                    if (((CheckBox)gvr.FindControl("chkDoAction")).Checked)
                    {
                        JOID = int.Parse(gvr.Cells[2].Text);
                        actionToTake = gvr.Cells[6].Text;

                        // 1: take the appropriate action at TK
                        if (actionToTake.ToLower() == "add" || actionToTake.ToLower() == "update")
                        {
                            UpdateTK(JOID); // send it over
                        }
                        else if (actionToTake.ToLower() == "delete")
                        {
                            // call the delete method
                            DeleteTK(JOID); // nuke it
                        }

                        // 2: update our Padawan table
                        u.MarkJobOpeningAsUpdated(JOID, DatabaseEnvironment.jd);
                    }
                }

                BindGrid();
            }
            catch (Exception ex)
            {
                lblError.Text += ex.Message;
            }
        }

        private void UpdateTK(int Job_Opening_Id)
        {
            // get the XML for the Vacancy out of the db
            Utilities u = new Utilities();
            string tkXML = u.GetTKXMLForJobOpening(Job_Opening_Id, DatabaseEnvironment.jd);

            // prep it
            tkXML = tkXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "");
            tkXML = tkXML.Replace("\r\n<Vacancy", "<Vacancy");
            byte[] b = Encoding.UTF8.GetBytes(tkXML);
            string encodedTKXML = Convert.ToBase64String(b);

            string tkVacancyURL = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKVacancyStagingURL"].ToString() : ConfigurationManager.AppSettings["TKVacancyProdURL"].ToString();
            HttpWebRequest vacRequest = (HttpWebRequest)WebRequest.Create(tkVacancyURL);
            vacRequest.Method = "POST";
            vacRequest.ContentType = "text/xml";
            XmlDocument soapVacancyEnvelopeXML = new XmlDocument();
            soapVacancyEnvelopeXML = CreateVacancySoapEnvelope(encodedTKXML, Job_Opening_Id);
            soapVacancyEnvelopeXML.Save(vacRequest.GetRequestStream());

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)vacRequest.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                lblError.Text += ex.Message;
            }

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(temp);
        }


        private void DeleteTK(int Job_Opening_Id)
        {
            string tkDeleteURL = TKEnvironment.tk == TKSystem.STAGING ? ConfigurationManager.AppSettings["TKDeleteDocumentStagingURL"].ToString() : ConfigurationManager.AppSettings["TKDeleteDocumentProdURL"].ToString();

            HttpWebRequest vacDelete = (HttpWebRequest)WebRequest.Create(tkDeleteURL);
            vacDelete.Method = "POST";
            vacDelete.ContentType = "text/xml";
            XmlDocument soapVacancyEnvelopeXML = new XmlDocument();
            soapVacancyEnvelopeXML = CreateDeleteSoapEnvelope(Job_Opening_Id);
            soapVacancyEnvelopeXML.Save(vacDelete.GetRequestStream());

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)vacDelete.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                lblError.Text += ex.Message;
            }

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(temp);
        }

        protected void grdTKStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                CheckBox cb = (CheckBox)e.Row.FindControl("chkDoAction");
                //CheckBox cb = (CheckBox)e.Row.Cells[7].FindControl("chkDoAction");
                //cb.CssClass = "actionbox";
                cb.Attributes.Add("class", "fish");
                cb.Checked = true;
                switch (e.Row.Cells[6].Text.ToLower())
                {
                    case "delete":
                        e.Row.BackColor = System.Drawing.Color.Pink;
                        break;
                    case "update":
                        e.Row.BackColor = System.Drawing.Color.FromArgb(255, 255, 153);
                        break;
                    case "add":
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                        break;
                }
            }
        }
    }
}