﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BOWebForms.Tools
{
    public partial class DeleteJobOpening : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            var searchTerm = txtJobOpening.Text.Trim();

            if (searchTerm == "")
                return;

            JMMDatabase jd = new JMMDatabase();
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    jd = JMMDatabase.DEV;
                    break;
                case "s":
                    jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    jd = JMMDatabase.PRODUCTION;
                    break;
            }

            Utilities u = new Utilities();
            DataTable dt = u.SearchForJobOpenings(searchTerm, jd);

            grdJobOpenings.DataSource = dt;
            grdJobOpenings.DataBind();

            pnl3.Visible = true;
        }

        protected void btn3_Click(object sender, EventArgs e)
        {
            lblJobOpeningId.Text = grdJobOpenings.SelectedRow.Cells[1].Text;
            pnl4.Visible = true;
        }

        protected void btnDeleteJobOpening_Click(object sender, EventArgs e)
        {
            JMMDatabase jd = new JMMDatabase();
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    jd = JMMDatabase.DEV;
                    break;
                case "s":
                    jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    jd = JMMDatabase.PRODUCTION;
                    break;
            }

            Utilities u = new Utilities();
            try
            {
                u.DeleteJobOpening(int.Parse(lblJobOpeningId.Text), jd);
                lblFinalMessage.Text = "Job Opening has been deleted";
            }
            catch (Exception ex)
            {
                lblFinalMessage.Text = "There was a problem deleting the Job Opening; error details as follows: " + ex.Message;
            }

            pnl5.Visible = true;
        }

        protected void btn5_Click(object sender, EventArgs e)
        {
            Response.Redirect("DeleteJobOpening");
        }
    }
}