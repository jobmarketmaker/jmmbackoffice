﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BOWebForms.Tools
{
    public partial class AddEmployer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            JMMDatabase jd = new JMMDatabase();
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    jd = JMMDatabase.DEV;
                    break;
                case "s":
                    jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    jd = JMMDatabase.PRODUCTION;
                    break;
            }

            Utilities u = new Utilities();

            if (txtEmployerName.Text.Trim() == "" || txtContactFirstName.Text.Trim() == "" || txtContactLastName.Text.Trim() == "" || txtContactEmail.Text.Trim() == "" || txtContactPassword.Text.Trim() == "")
            {
                lblError.Text = "Some required data is missing!";
                return;
            }
            if (txtContactPassword.Text.Trim().Length < 5)
            {
                lblError.Text = "The password must be at least 5 characters long.";
                return;
            }
            try
            {
                int success = u.VerifyEmailAvailable(txtContactEmail.Text.Trim(), txtSubdomainPrefix.Text.Trim(), jd);
                if (success == 0)
                {
                    lblError.Text = "Sorry boss, but the email you have chosen is already taken.";
                    return;
                }
            }
            catch (Exception ex) {
                lblFinalMessage.Text = "There was a problem verifying the existence of that email address; error details as follows: " + ex.Message;
            }

            lblError.Text = ""; // all is well, so clear any existing error message;

            try
            {
                int employer_Id = u.AddEmployer(txtEmployerName.Text.Trim(), txtContactFirstName.Text.Trim(), txtContactLastName.Text.Trim(), txtContactEmail.Text.Trim(), txtContactPhone.Text.Trim(), txtContactPassword.Text.Trim(), txtSubdomainPrefix.Text.Trim(), jd);
                lblFinalMessage.Text = "Employer has been added; its Employer_Id is " + employer_Id.ToString();
            }
            catch (Exception ex)
            {
                lblFinalMessage.Text = "There was a problem adding the Employer; error details as follows: " + ex.Message;
            }

            pnl3.Visible = true;
        }

        protected void btnRestart_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddEmployer");
        }
    }
}