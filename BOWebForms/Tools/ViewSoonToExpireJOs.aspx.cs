﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BOWebForms.Tools
{
    public partial class ViewSoonToExpireJOs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DatabaseEnvironment.DatabaseSelected += DatabaseEnvironment_DatabaseSelected;
        }

        private void DatabaseEnvironment_DatabaseSelected(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            Utilities u = new Utilities();
            try
            {
                DataTable dt = u.ViewSoonToExpireJobOpenings(int.Parse(txtNumberOfDays.Text), DatabaseEnvironment.jd);
                grdJobOpenings.DataSource = dt;
                grdJobOpenings.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = "There was a problem retrieving the report; error details as follows: " + ex.Message;
            }

            pnl3.Visible = true;
        }

        protected void btnToCSV_Click(object sender, EventArgs e)
        {
            Utilities u = new Utilities();
            DataTable dt = u.ViewSoonToExpireJobOpenings(int.Parse(txtNumberOfDays.Text), DatabaseEnvironment.jd);

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                  Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                  string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }


            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=soontoexpire_" + System.DateTime.Now.ToString("yyyyMMdd") + "_" + txtNumberOfDays.Text.ToString() + "days.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            //StringBuilder sBuilder = new System.Text.StringBuilder();
            //for (int index = 0; index < grdJobOpenings.Columns.Count; index++)
            //{
            //    sBuilder.Append(grdJobOpenings.Columns[index].HeaderText + ',');
            //}
            //sBuilder.Append("\r\n");
            //for (int i = 0; i < grdJobOpenings.Rows.Count; i++)
            //{
            //    for (int k = 0; k < grdJobOpenings.HeaderRow.Cells.Count; k++)
            //    {
            //        sBuilder.Append(grdJobOpenings.Rows[i].Cells[k].Text.Replace(",", "") + ",");
            //    }
            //    sBuilder.Append("\r\n");
            //}
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}