﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BOWebForms.Classes
{
    public enum JMMDatabase
    {
        DEV,
        STAGING,
        SANDBOX,
        PRODUCTION
    }

    public enum TKSystem
    {
        STAGING,
        PRODUCTION
    }

    public class Utilities
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public Utilities()
        {
            //if (HttpContext.Current.Session["JMM"] == null)
            //{
            //    css = settings["JMM-Dev"];
            //}
            //else
            //{
            //    switch (HttpContext.Current.Session["JMM"].ToString())
            //    {
            //        case "dev":
            //            css = settings["JMM-Dev"];
            //            break;
            //        case "staging":
            //            css = settings["JMM-Staging"];
            //            break;
            //        case "sandbox":
            //            css = settings["JMM-Sandbox"];
            //            break;
            //        case "prod":
            //            css = settings["JMM-Nov6Beta"];
            //            break;
            //    }
            //}
        }

        private ConnectionStringSettings GetCss(JMMDatabase DatabaseToConnectTo)
        {
            ConnectionStringSettings retCss = new ConnectionStringSettings();
            switch (DatabaseToConnectTo)
            {
                case JMMDatabase.DEV:
                    retCss = settings["JMM-Dev"];
                    break;
                case JMMDatabase.STAGING:
                    retCss = settings["JMM-Staging"];
                    break;
                case JMMDatabase.SANDBOX:
                    retCss = settings["JMM-Sandbox"];
                    break;
                case JMMDatabase.PRODUCTION:
                    retCss = settings["JMM-Nov6Beta"];
                    break;
            }
            return retCss;
        }

        public DataTable SearchForCandidates(string SearchTerm, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            #region old
            //switch (DatabaseToConnectTo) {
            //    case JMMDatabase.DEV:
            //        css = settings["JMM-Dev"];
            //        break;
            //    case JMMDatabase.STAGING:
            //        css = settings["JMM-Staging"];
            //        break;
            //    case JMMDatabase.SANDBOX:
            //        css = settings["JMM-Sandbox"];
            //        break;
            //    case JMMDatabase.PRODUCTION:
            //        css = settings["JMM-Nov6Beta"];
            //        break;
            //}
            #endregion

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_CandidateSearch", conn);
            cmd.Parameters.Add(new SqlParameter("SearchTerm", SearchTerm));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public DataTable SearchForEmployerContacts(string SearchTerm, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_EmployerContactSearch", conn);
            cmd.Parameters.Add(new SqlParameter("SearchTerm", SearchTerm));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public DataTable SearchForEmployers(string SearchTerm, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_EmployerSearch", conn);
            cmd.Parameters.Add(new SqlParameter("SearchTerm", SearchTerm));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public void UpdateEmployerBrandingInfo(int Employer_Id, bool Branding, string SubdomainPrefix, JMMDatabase DatabaseToConnectTo)
        {
            if (SubdomainPrefix.Trim() == "")
            {
                SubdomainPrefix = "app";
            }

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_UpdEmployerBrandingInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Employer_Id", Employer_Id));
            cmd.Parameters.Add(new SqlParameter("Branding", Branding));
            cmd.Parameters.Add(new SqlParameter("Subdomain_Prefix", SubdomainPrefix));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }


        public void UpdateCandidatePassword(int Candidate_Id, string Password, JMMDatabase DatabaseToConnectTo)
        {
            // .Net implementation of BCrypt
            // thank you http://www.codeproject.com/Articles/475262/UseplusBCryptplustoplusHashplusYourplusPasswords and http://derekslager.com/blog/attachment.ashx/bcrypt-dotnet-strong-password-hashing-for-dotnet-and-mono/BCrypt.cs
            string mySalt = BCrypt.GenerateSalt(); //"bacon";
            string myHash = BCrypt.HashPassword(Password, mySalt);

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_UpdCandidatePassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", Candidate_Id));
            cmd.Parameters.Add(new SqlParameter("Password", myHash)); // Password));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }


        public void UpdateEmployerContactPassword(int Emp_Contact_Id, string Password, JMMDatabase DatabaseToConnectTo)
        {
            string mySalt = BCrypt.GenerateSalt(); //"bacon";
            string myHash = BCrypt.HashPassword(Password, mySalt);

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_UpdEmployerContactPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Emp_Contact_Id", Emp_Contact_Id));
            cmd.Parameters.Add(new SqlParameter("Password", myHash)); // Password));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }


        public void DeleteCandidate(int Candidate_Id, JMMDatabase DatabaseToConnectTo)
        {
            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_DelCandidate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", Candidate_Id));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }

        //public void SaveBackOfficeConnections(BackOfficeConnections BOConnections)
        //{
        //    HttpContext.Current.Session["TK"] = BOConnections.TextKernelConnection;
        //    HttpContext.Current.Session["JMM"] = BOConnections.JMMDBConnection;
        //    //Session["blah"] = "a";
        //    string s = "";
        //}

        public DataTable SearchForJobOpenings(string SearchTerm, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_JobOpeningSearch", conn);
            cmd.Parameters.Add(new SqlParameter("SearchTerm", SearchTerm));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }

        public void DeleteJobOpening(int Job_Opening_Id, JMMDatabase DatabaseToConnectTo)
        {
            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_DelJobOpening", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", Job_Opening_Id));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }

        public int VerifyEmailAvailable(string Email, string SubdomainPrefix, JMMDatabase DatabaseToConnectTo)
        {
            int retVal = 0;

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_VerifyEmailAvailable", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Email", Email));
            cmd.Parameters.Add(new SqlParameter("@App_Subdomain_Prefix", SubdomainPrefix));

            conn.Open();
            retVal = (int)cmd.ExecuteScalar();

            return retVal;
        }

        public int AddEmployer(string EmployerName, string ContactFirstName, string ContactLastName, string ContactEmail, string ContactPhone, string Password, string SubdomainPrefix, JMMDatabase DatabaseToConnectTo)
        {
            int retVal = 0;

            if (SubdomainPrefix.Trim() == "")
            {
                SubdomainPrefix = "app";
            }

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_InsEmployerAndContactInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Employer_Name", EmployerName));
            cmd.Parameters.Add(new SqlParameter("Emp_Contact_First_Name", ContactFirstName));
            cmd.Parameters.Add(new SqlParameter("Emp_Contact_Last_Name", ContactLastName));
            cmd.Parameters.Add(new SqlParameter("Email_Address", ContactEmail));
            cmd.Parameters.Add(new SqlParameter("Phone_Number", ContactPhone));
            cmd.Parameters.Add(new SqlParameter("Password", BCrypt.HashPassword(Password, BCrypt.GenerateSalt())));
            cmd.Parameters.Add(new SqlParameter("IsAdmin", 1));
            cmd.Parameters.Add(new SqlParameter("Subdomain_Prefix", SubdomainPrefix));
            cmd.Parameters.Add(new SqlParameter("Employer_Id_Out", SqlDbType.Int));
            cmd.Parameters.Add(new SqlParameter("Emp_Contact_Id_Out", SqlDbType.Int));

            cmd.Parameters["Employer_Id_Out"].Direction = ParameterDirection.Output;
            cmd.Parameters["Emp_Contact_Id_Out"].Direction = ParameterDirection.Output;

            conn.Open();
            cmd.ExecuteNonQuery();

            int employer_Id = int.Parse(cmd.Parameters["Employer_Id_Out"].Value.ToString());

            cmd = null;
            conn.Close();
            conn = null;

            retVal = employer_Id;
            return retVal;
        }


        public DataTable ViewSoonToExpireJobOpenings(int DaysNoticeThreshold, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_Reporting_JobOpenings_SoonToExpire", conn);
            cmd.Parameters.Add(new SqlParameter("DaysNoticeThreshold", DaysNoticeThreshold));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public DataTable JobOpeningInternalAudit(JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_JobOpeningInternalAudit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public DataTable ViewTKStatus(string JobOpeningIDsAtTK, JMMDatabase DatabaseToConnectTo)
        {
            DataTable dt = new DataTable();

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_BackOffice_TKCurrencyLoad", conn);
            cmd.Parameters.Add(new SqlParameter("JOIDsAtTK", JobOpeningIDsAtTK));
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            cmd = null;
            conn.Close();
            conn = null;

            return dt;
        }


        public void MarkJobOpeningAsUpdated(int Job_Opening_Id, JMMDatabase DatabaseToConnectTo)
        {
            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_UpdUploadValue", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", Job_Opening_Id));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }


        public string GetTKXMLForJobOpening(int Job_Opening_Id, JMMDatabase DatabaseToConnectTo)
        {
            string retVal = "";

            css = GetCss(DatabaseToConnectTo);
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_GenerateJobOpeningXMLForTK", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", Job_Opening_Id));

            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                retVal = dr["TKXML"].ToString();
            }

            dr.Close();

            cmd = null;
            conn.Close();
            conn = null;

            return retVal;
        }
    }
}