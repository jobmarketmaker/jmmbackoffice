﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BOWebForms.Models;
using System.Web.Security;

namespace BOWebForms.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            //if (!String.IsNullOrEmpty(returnUrl))
            //{
            //    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            //}
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, true, shouldLockout: false);

                //bool success = false;

                //if (Email.Text == "jmmadmin" && Password.Text == "bingobangobongo")
                //    success = true;

                //if (success)
                //{
                //    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                //    //FormsAuthentication.RedirectFromLoginPage(Email.Text, true);

                //    //FormsAuthenticationTicket tkt;
                //    //string cookiestr;
                //    //HttpCookie ck;
                //    //tkt = new FormsAuthenticationTicket(1, Email.Text, DateTime.Now, DateTime.Now.AddMinutes(30), true, "your custom data");
                //    //cookiestr = FormsAuthentication.Encrypt(tkt);
                //    //ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                //    ////if (chkPersistCookie.Checked)
                //    //ck.Expires = tkt.Expiration;
                //    //ck.Path = FormsAuthentication.FormsCookiePath;
                //    //Response.Cookies.Add(ck);

                //    //string strRedirect;
                //    //strRedirect = Request["ReturnUrl"];
                //    //if (strRedirect == null)
                //    //    strRedirect = "default.aspx";
                //    //Response.Redirect(strRedirect, true);
                //}
                //else
                //{
                //    FailureText.Text = "Invalid login attempt";
                //    ErrorMessage.Visible = true;
                //}

                //result = SignInStatus.Success;

                switch (result)
                {
                    case SignInStatus.Success:
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        break;
                    //case SignInStatus.LockedOut:
                    //    Response.Redirect("/Account/Lockout");
                    //    break;
                    //case SignInStatus.RequiresVerification:
                    //    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                    //                                    Request.QueryString["ReturnUrl"],
                    //                                    RememberMe.Checked),
                    //                      true);
                    //    break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }
    }
}