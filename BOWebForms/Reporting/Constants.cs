﻿#region Header

// The sole owner of this code is Dundas Data Visualization Inc.
// Copyright (c) 2014 Dundas Data Visualization Inc.
// All rights reserved.

#endregion Header

using System;

namespace NetViewerIntegrationSample
{
    /// <summary>
    /// This class contains constants used by the viewer integration sample.
    /// </summary>
    internal static class Constants
    {
        /// <summary>
        /// The Dundas BI server URI
        /// </summary>
        public static readonly Uri DundasBIServerUri = new Uri("https://analytics.jobmarketmaker.com/"); //("http://137.117.35.117/");

        /// <summary>
        /// The Dundas BI viewer username.
        /// </summary>
        public const string DundasBIViewerUsername = "admin"; // "jmmdundasuser_12757"; // "admin"; // "viewer"; // "viewer";
        //public const string DundasBIViewerUsername2 = "viewer"; // "viewer"; // "viewer";
        //public const string DundasBIViewerUsername3 = "bg"; // "viewer"; // "viewer";

        /// <summary>
        /// The Dundas BI viewer password.
        /// </summary>
        public const string DundasBIViewerPassword = "ardom2015!"; // "jmmardom2015!dundas"; // "ardom2015!"; //"1234";

        /// <summary>
        /// The Dundas BI dashboard identifier.
        /// </summary>
        //public const string DundasBIDashboardId = "b71d2280-16ad-4475-a095-fc4d39f828fb"; // this is the 3-pronger       // "d4f883df-5d6a-4db7-9c21-90df9b7e4902"; //"67c0aa44-8d1f-4b22-b163-a5646e183a90";
        //public const string DundasBIDashboardIdA = "2fe9468f-690b-48f6-9709-e958a9ca2424"; // JMM App, dash_A
        //public const string DundasBIDashboardIdB = "1b39a0b2-6111-47e9-b852-6cd56b6ff29b"; // JMM App, dash_B
        //public const string DundasBIDashboardIdC = "3367f354-6223-4ad3-b460-836ec9ddf858"; // JMM App, dash_C
        //public const string DundasBIDashboardIdD = "b050b12b-70ce-417f-a58b-e59caa5f05ca"; // JMM App, dash_D
        //public const string DundasBIDashboardIdE = "18696bf2-3980-47e8-8c56-8890e47dca9d"; // JMM App, dash_E
        public const string DundasBIDashboardIdAnalytics = "8ed33b30-a4eb-474a-b783-9fea8d730732";
        public const string DundasBIDashboardIdEEOC = "81febfe8-f44f-4dd9-a41c-fb7363f1c88b";
        public const string DundasBIDashboardIdScatter = "9d5d766c-8f7b-41d2-aa33-cf5224b2630a";
        public const string DundasBIDashboardIdSPARC = "9a158bb2-5fb2-4f7c-a793-07beb9b20ce6";

        public const string DundasBIDashboardIdLandingPageTest = "5b0584aa-88c9-4dfd-97be-16942326c391";

        public const string DundasBIDashboardIdJOReq = "6b02746a-5aff-4899-adef-ac1cee8eaeab";

        public const string DundasBIDashboardIdJobTemplateMapping = "24fa1354-c16b-4ddd-a19c-8cc2a5aae508";

        public const string DundasBIDashboardIdProper = "41c6a7af-c62b-4dd3-bfe8-f0de46e4ad62";

        public const string DundasBIFFT = "4f46f95a-d285-4ce7-ae12-3c1e9d45f45d";

        public const string DundasBITimeToAccept = "f4fbdda2-4734-40d5-8326-b5b4d601b281";

        public const string DundasBISpeed = "6a87398d-b9b5-48de-9286-ac935dc79593";
        /// <summary>
        /// The view options represent are defined in the dundas.viewoptions.js in the instance folder.
        /// Possibilites include: none, viewonlynoplayground, viewonly, menuonly, menutoolbar, 
        /// and menutoolbartaskbar.
        /// </summary>
        public const string ViewOptions = "viewonly";
    }
}