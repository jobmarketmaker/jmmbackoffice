﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace NetViewerIntegrationSample
{
    public partial class Employer_Dashboard_Notifications : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadIFrame("tuw");
            }
        }

        private void LoadIFrame(string EmployerName)
        {
            #region Creating LogOn Token Options

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            // The data represents a json serialized 
            // Dundas.BI.WebApi.Models.LogOnTokenOptions object.

            string user = "";
            if (EmployerName == "tuw")
                user = Constants.DundasBIViewerUsername;

            var logOnTokenOptions =
                new
                {
                    accountName = user, // Constants.DundasBIViewerUsername,
                    password = Constants.DundasBIViewerPassword,
                    isWindowsLogOn = false
                };

            // Create string used in request body.
            string logOnTokenOptionsAsString =
                javaScriptSerializer.Serialize(logOnTokenOptions);

            // Create the REST login url.
            string restLoginUrl =
                Path.Combine(
                    Constants.DundasBIServerUri.ToString(),
                    "Api/LogOn/Token/"
                );

            #endregion Creating LogOn Token Options

            #region Create the HttpWebRequest

            // Create the HttpWebRequest
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(restLoginUrl);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = logOnTokenOptionsAsString.Length;

            using (StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII))
            {
                requestWriter.Write(logOnTokenOptionsAsString);
            }

            #endregion Create the HttpWebRequest

            #region Get the Web Response

            WebResponse webResponse = request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            string response = responseReader.ReadToEnd();
            responseReader.Close();

            for (int i = 0; i < webResponse.Headers.Count; ++i)
            {
                Console.WriteLine("\nHeader Name:{0}, Value :{1}", webResponse.Headers.Keys[i], webResponse.Headers[i]);

            }

            #endregion Get the Web Response

            #region Process Response and set iframe src

            Dictionary<string, object> obj =
                (Dictionary<string, object>)javaScriptSerializer.Deserialize(response, typeof(object));

            Guid logOnToken = new Guid(obj["logOnToken"].ToString());
            string logOnFailureReason = obj["logOnFailureReason"].ToString();
            string message = obj["message"].ToString();

            if (logOnFailureReason.Equals("None"))
            {
                iFrame.Attributes.Clear();
                iFrame.Attributes.Add(
                    "src",
                    string.Concat(
                        Constants.DundasBIServerUri.ToString(),
                        "Dashboard/",
                        Constants.DundasBIDashboardIdProper,
                        "?e=false&vo=", Constants.ViewOptions,
                        "&logonTokenId=", logOnToken.ToString()
                        )
                    );
            }

            #endregion #region Process Response and set iframe src
        }
    }
}