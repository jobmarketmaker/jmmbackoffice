﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="employer_dashboard_jobtemplatemapping.aspx.cs" Inherits="NetViewerIntegrationSample.Employer_Dashboard_JobTemplateMapping" %>
<%@ Register Src="~/Controls/ReportsQuickLinks.ascx" TagPrefix="uc1" TagName="ReportsQuickLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- CSS Files -->
    <%--<link rel="stylesheet" type="text/css" href="sample.css" />--%>

    <style type="text/css">
        iframe{
            /*position:relative;
            left:200px;*/
            width:100%;
            height:850px;
            border:none;
            overflow-x:hidden;
        }
    </style>

	<title>Job Market Maker - Sample Job Template Mapping Report</title>	
</head>
<body style="width:100%; margin:0px; padding:0px; border:0px;">

    <form id="form1" runat="server" onload="Page_Load">
        <asp:ScriptManager runat="server">
            <Scripts>
                <asp:ScriptReference Name="jquery" />
            </Scripts>
        </asp:ScriptManager>

        <div style="background-image:url('/images/mockup_employer_dashboard.png'); width:1577px; height:345px;"></div>

        <iframe id="iFrame" runat="server" />

        <br /><br />

    </form>
     
<uc1:ReportsQuickLinks runat="server" ID="ReportsQuickLinks" />

</body>
</html>
