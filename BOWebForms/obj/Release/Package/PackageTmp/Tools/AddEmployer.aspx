﻿<%@ Page Title="Add Employer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddEmployer.aspx.cs" Inherits="BOWebForms.Tools.AddEmployer" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .tag {
            font-weight:bold;
            width:175px;
        }
    </style>

    <h1>Add Employer</h1>

    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

    <asp:Panel ID="pnl1" runat="server">

        <h3>Step 1: Select a database environment</h3>
        <asp:DropDownList ID="ddlDB" runat="server">
            <asp:ListItem Selected="True" Value="d">Development</asp:ListItem>
            <asp:ListItem Value="s">Staging</asp:ListItem>
            <asp:ListItem Value="x">Sandbox</asp:ListItem>
            <asp:ListItem Value="p">Production</asp:ListItem>
        </asp:DropDownList>
        <br /><br />
        <asp:Button ID="btn1" runat="server" Text="Next >>" OnClick="btn1_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl2" runat="server" Visible="false">

        <hr />
        <h3>Step 2: Enter the Employer fields</h3>

        <table>
            <tr>
                <td class="tag">Employer Name</td>
                <td><asp:TextBox ID="txtEmployerName" runat="server" Width="200" MaxLength="200" /></td>
            </tr>
            <tr>
                <td class="tag">Contact First Name</td>
                <td><asp:TextBox ID="txtContactFirstName" runat="server" Width="200" MaxLength="50" /></td>
            </tr>
            <tr>
                <td class="tag">Contact Last Name</td>
                <td><asp:TextBox ID="txtContactLastName" runat="server" Width="200" MaxLength="50" /></td>
            </tr>
            <tr>
                <td class="tag">Contact Email</td>
                <td><asp:TextBox ID="txtContactEmail" runat="server" Width="200" MaxLength="200" /></td>
            </tr>
            <tr>
                <td class="tag">Contact Phone</td>
                <td><asp:TextBox ID="txtContactPhone" runat="server" Width="200" MaxLength="20" /></td>
            </tr>
            <tr>
                <td class="tag">Contact Password</td>
                <td><asp:TextBox ID="txtContactPassword" runat="server" Width="200" MaxLength="100" /></td>
            </tr>
            <tr>
                <td class="tag">Subdomain Prefix</td>
                <td><asp:TextBox ID="txtSubdomainPrefix" runat="server" Width="200" MaxLength="25" /></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
        
    </asp:Panel>


    <asp:Panel ID="pnl3" runat="server" Visible="false">

        <hr />
        <h3><asp:Label ID="lblFinalMessage" runat="server" /></h3>
        <asp:Button ID="btnRestart" runat="server" Text="Add another Employer" OnClick="btnRestart_Click" />

    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    if (document.getElementById('MainContent_btnSave')) {
                        $("#MainContent_btnSave").click();
                    }
                }
            });
        });

    </script>

</asp:Content>
