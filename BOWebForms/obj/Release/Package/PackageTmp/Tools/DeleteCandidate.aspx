﻿<%@ Page Title="Delete Candidate" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DeleteCandidate.aspx.cs" Inherits="BOWebForms.Tools.DeleteCandidate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Delete Candidate</h1>

    <asp:Panel ID="pnl1" runat="server">

        <h3>Step 1: Select a database environment</h3>
        <asp:DropDownList ID="ddlDB" runat="server">
            <asp:ListItem Selected="True" Value="d">Development</asp:ListItem>
            <asp:ListItem Value="s">Staging</asp:ListItem>
            <asp:ListItem Value="x">Sandbox</asp:ListItem>
            <asp:ListItem Value="p">Production</asp:ListItem>
        </asp:DropDownList>
        <br /><br />
        <asp:Button ID="btn1" runat="server" Text="Next >>" OnClick="btn1_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl2" runat="server" Visible="false">

        <hr />
        <h3>Step 2: Enter Candidate_Id, part of name, or part of email address</h3>
        <asp:TextBox ID="txtCandidate" runat="server" MaxLength="100" />
        <br /><br />
        <asp:Button ID="btn2" runat="server" Text="Next >>" OnClick="btn2_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl3" runat="server" Visible="false">

        <hr />
        <h3>Step 3: Select the Candidate you wish to delete</h3>
        <asp:GridView ID="grdCandidates" runat="server" AutoGenerateSelectButton="True">
            <SelectedRowStyle BackColor="Yellow" />
        </asp:GridView>
        <br />
        <asp:Button ID="btn3" runat="server" Text="Next >>" OnClick="btn3_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl4" runat="server" Visible="false">

        <hr />
        <h3>Step 4: Confirm that you want to delete Candidate_Id <asp:Label ID="lblCandidateId" runat="server" />. THIS CANNOT BE UNDONE!!!</h3>
        <asp:Button ID="btnDeleteCandidate" runat="server" Text="Yes, do it already man!" OnClick="btnDeleteCandidate_Click" />
        
    </asp:Panel>


    <asp:Panel ID="pnl5" runat="server" Visible="false">

        <hr />
        <h3><asp:Label ID="lblFinalMessage" runat="server" /></h3>
        <asp:Button ID="btnRestart" runat="server" Text="Start again" OnClick="btn5_Click" />

    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    if (document.getElementById('MainContent_btn2')) {
                        $("#MainContent_btn2").click();
                    }
                }
            });
        });

    </script>

</asp:Content>
