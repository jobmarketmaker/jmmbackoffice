﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="BOWebForms.Tools._default" %>
<%@ Register Src="~/Controls/ToolList.ascx" TagPrefix="uc1" TagName="ToolList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Below are the tools available</h1>

    <uc1:ToolList runat="server" id="ToolList" />

</asp:Content>
