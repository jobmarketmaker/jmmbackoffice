﻿<%@ Page Title="Reset Employer Branding Info" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetEmployerBrandingInfo.aspx.cs" Inherits="BOWebForms.Tools.ResetEmployerBrandingInfo" %>
<%@ Register Src="~/Controls/DatabaseEnvironment.ascx" TagPrefix="uc1" TagName="DatabaseEnvironment" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Reset Employer Branding Info</h1>

    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

    <asp:Panel ID="pnl1" runat="server">

        <uc1:DatabaseEnvironment runat="server" id="DatabaseEnvironment" />

    </asp:Panel>


    <asp:Panel ID="pnl2" runat="server" Visible="false">

        <hr />
        <h3>Step 2: Enter Employer_Id or part of Employer name</h3>
        <asp:TextBox ID="txtEmployer" runat="server" MaxLength="100" />
        <br /><br />
        <asp:Button ID="btn2" runat="server" Text="Next >>" OnClick="btn2_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl3" runat="server" Visible="false">

        <hr />
        <h3>Step 3: Select the Employer whose branding information you wish to reset</h3>
        <asp:GridView ID="grdEmployers" runat="server" AutoGenerateSelectButton="True">
            <SelectedRowStyle BackColor="Yellow" />
        </asp:GridView>
        <br />
        <asp:Button ID="btn3" runat="server" Text="Next >>" OnClick="btn3_Click" />

    </asp:Panel>


    <asp:Panel ID="pnl4" runat="server" Visible="false">

        <hr />
        <h3>Step 4: Enter the new branding information for Employer_Id <asp:Label ID="lblEmployerId" runat="server" /></h3>
        Branding on? <asp:CheckBox ID="chkBranding" runat="server" />
        <br />
        Subdomain prefix <asp:TextBox ID="txtSubdomainPrefix" runat="server" MaxLength="100" />
        <asp:Button ID="btnSave" runat="server" Text="Finish" OnClick="btn4_Click" />
        
    </asp:Panel>


    <asp:Panel ID="pnl5" runat="server" Visible="false">

        <hr />
        <h3><asp:Label ID="lblFinalMessage" runat="server" /></h3>
        <asp:Button ID="btnRestart" runat="server" Text="Start again" OnClick="btn5_Click" />

    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function () {
            //$(document).keypress(function (event) {
            //    if (event.which == 13) {
            //        event.preventDefault();
            //        if (document.getElementById('MainContent_btnSavePassword')) {
            //            $("#MainContent_btnSavePassword").click();
            //        }
            //        else if (document.getElementById('MainContent_btn2')) {
            //            $("#MainContent_btn2").click();
            //        }
            //    }
            //});
        });

    </script>

</asp:Content>
