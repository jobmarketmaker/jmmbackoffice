﻿<%@ Page Title="Check TK Currency" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckTKCurrency.aspx.cs" Inherits="BOWebForms.Tools.CheckTKCurrency" %>
<%@ Register Src="~/Controls/DatabaseEnvironment.ascx" TagPrefix="uc1" TagName="DatabaseEnvironment" %>
<%@ Register Src="~/Controls/TKEnvironment.ascx" TagPrefix="uc1" TagName="TKEnvironment" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Check TK Currency</h1>

    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>

    <asp:Panel ID="pnl1" runat="server">

        <uc1:DatabaseEnvironment runat="server" id="DatabaseEnvironment" />

    </asp:Panel>


    <asp:Panel ID="pnl2" runat="server" Visible="false">

        <hr />

        <uc1:TKEnvironment runat="server" id="TKEnvironment" />

    </asp:Panel>


    <asp:Panel ID="pnl3" runat="server" Visible="false">

        <style type="text/css">
            .bold {
                font-weight:bold;
            }
            .cellcenter{
                text-align:center;
            }
        </style>

        <hr />
        <h3>Here's the situation</h3>
        <br />

        <asp:Button ID="btnAct" runat="server" Text="Take actions" OnClick="btnAct_Click" />
        <br /><br />
        <span style="font-style:italic; color:red;">Please note: updates in the TK system may take up to 15 minutes to process.</span>

<%--        <div style="height: 250px; overflow-y:scroll;">--%>
            <table style="width:100%;">
                <tr>
                    <td style="width:100%; text-align:right">
                        
                    </td>
                </tr>
            </table>
            <asp:GridView ID="grdTKStatus" runat="server" Font-Size="Smaller" AutoGenerateColumns="false" OnRowDataBound="grdTKStatus_RowDataBound" Width="100%">
                <Columns>
                    <asp:BoundField DataField="Employer_Name" HeaderText="Employer" />
                    <asp:BoundField DataField="Job_Title" HeaderText="Job_Title" />
                    <asp:BoundField DataField="Job_Opening_Id" HeaderText="JOID" />
                    <asp:BoundField DataField="Last_Modified_Date" HeaderText="Last Modified" />
                    <asp:CheckBoxField DataField="ExistsInJMM" HeaderText="In JMM" ReadOnly="true" />
                    <asp:CheckBoxField DataField="ExistsInTK" HeaderText="In TK" ReadOnly="true" />
                    <asp:BoundField DataField="RecommendedAction" HeaderText="Recommended Action" />
                    <asp:TemplateField HeaderStyle-CssClass="cellcenter" ItemStyle-CssClass="cellcenter">
                        <HeaderTemplate>
                            Do this
                            <br />
                            <input type="checkbox" id="cbUncheckAll" checked="checked" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDoAction" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
<%--        </div>--%>

    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#cbUncheckAll").change(function () {
                var status = $("#cbUncheckAll").is(':checked');
                $("input[type='checkbox']:not(:disabled)").prop('checked', status);
            });
        });

    </script>

</asp:Content>
