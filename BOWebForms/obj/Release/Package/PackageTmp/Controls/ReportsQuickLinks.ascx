﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportsQuickLinks.ascx.cs" Inherits="BOWebForms.Controls.ReportsQuickLinks" %>

<div style="position:absolute; top:5px; left:5px; width:25px; height:25px;">
    <button id="btnExpand" style="width:25px; height:25px;">+</button>
</div>
<div id="divLinks" style="position:absolute; top:5px; left:35px; width:350px; height: 180px; display:none; background-color: lightgray; font-family:Arial; font-size:12px;">
    <ul>
        <li><a href="/Reporting/Employer_Dashboard">Analytics</a></li>
        <li><a href="/Reporting/Employer_Dashboard_EEOC">EEOC</a></li>
        <li><a href="/Reporting/Employer_Dashboard_Scatter">Resume Scores vs JMM Assessment Scores</a></li>
        <li><a href="/Reporting/Employer_Dashboard_SPARC">SPARC</a></li>
        <li><a href="/Reporting/Employer_Dashboard_LandingPageTest">Reports Landing Page</a></li>
        <li><a href="/Reporting/Employer_Dashboard_JOReq">Job Opening Requirements</a></li>
        <li><a href="/Reporting/Employer_Dashboard_JobTemplateMapping">Job Template Mapping</a></li>
        <li><a href="/Reporting/Employer_Dashboard_Notifications">Dashboard</a></li>
        <li><a href="/Reporting/Employer_Dashboard_FFT">FFT</a></li>
        <li><a href="/Reporting/Employer_Dashboard_TimeToAccept">Time to Accept</a></li>
        <li><a href="/Reporting/Employer_Dashboard_Speed">Speed</a></li>
    </ul>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $("#btnExpand").click(function () {
            if ($("#btnExpand").html() == "+") {
                $("#divLinks").show('fast');
                $("#btnExpand").html("-");
            }
            else {
                $("#divLinks").hide('fast');
                $("#btnExpand").html("+");
            }
        });
    });

</script>