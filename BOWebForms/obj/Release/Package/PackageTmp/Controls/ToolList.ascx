﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ToolList.ascx.cs" Inherits="BOWebForms.Controls.ToolList" %>

<div>

<%--<h2>Settings</h2>
<ul>
    <li><a href="/Tools/ChangeConnections.aspx">Change connections</a></li>
</ul>--%>

<h2>General Tools</h2>
<ul>
    <li><a href="/Tools/ResetCandidatePassword.aspx">Reset a Candidate Password</a></li>
    <li><a href="/Tools/ResetEmployerContactPassword.aspx">Reset an Employer Contact Password</a></li>
    <li><a href="/Tools/AddEmployer.aspx">Add an Employer</a></li>
    <%--<li>Export resumes</li><a href="/Tools/ExportResumes.aspx"></a>--%>
    <%--<li>Upload a Job Board Image</li><a href="/Tools/UploadJobBoardImage.aspx"></a>--%>
    <li><a href="/Tools/DeleteCandidate.aspx">Delete a Candidate</a></li>
    <li><a href="/Tools/DeleteJobOpening.aspx">Delete a Job Opening</a></li>
    <%--<li>Delete an Employer</li>--%>
    <li><a href="/Tools/ViewSoonToExpireJOs.aspx">View Soon-to-Expire Job Openings</a></li>
    <li><a href="/Tools/ResetEmployerBrandingInfo.aspx">Reset an Employer's Branding Info</a></li>
</ul>

<h2>TextKernel Tools</h2>
<ul>
    <li><a href="/Tools/CheckTKCurrency.aspx">Check TK Currency</a></li>
    <%--<li>Create a one-time Candidate + Job Opening score</li>--%>
</ul>

<h2>Sample Employer Reports</h2>
<span style="color:maroon; font-style:italic;">Please note: all these sample reports show Trident United Way production data</span>
<ul>
    <li><a href="/Reporting/Employer_Dashboard" target="_blank">Analytics</a></li>
    <li><a href="/Reporting/Employer_Dashboard_EEOC" target="_blank">EEOC</a></li>
    <li><a href="/Reporting/Employer_Dashboard_Scatter" target="_blank">Resume Scores vs JMM Assessment Scores</a></li>
    <li><a href="/Reporting/Employer_Dashboard_SPARC" target="_blank">SPARC</a></li>
    <li><a href="/Reporting/Employer_Dashboard_LandingPageTest" target="_blank">Reports Landing Page</a></li>
    <li><a href="/Reporting/Employer_Dashboard_JOReq" target="_blank">Job Opening Requirements</a></li>
    <li><a href="/Reporting/Employer_Dashboard_JobTemplateMapping" target="_blank">Job Template Mapping</a></li>
    <li><a href="/Reporting/Employer_Dashboard_Notifications" target="_blank">Dashboard</a></li>
    <li><a href="/Reporting/Employer_Dashboard_FFT" target="_blank">FFT</a></li>
    <li><a href="/Reporting/Employer_Dashboard_TimeToAccept" target="_blank">Time to Accept</a></li>
    <li><a href="/Reporting/Employer_Dashboard_Speed" target="_blank">Speed</a></li>
</ul>

</div>