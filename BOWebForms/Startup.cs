﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BOWebForms.Startup))]
namespace BOWebForms
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
