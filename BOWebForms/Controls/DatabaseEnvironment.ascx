﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatabaseEnvironment.ascx.cs" Inherits="BOWebForms.Controls.DatabaseEnvironment" %>

<h3>Select a database environment</h3>
<asp:DropDownList ID="ddlDB" runat="server">
    <asp:ListItem Selected="True" Value="d">Development</asp:ListItem>
    <asp:ListItem Value="s">Staging</asp:ListItem>
    <asp:ListItem Value="x">Sandbox</asp:ListItem>
    <asp:ListItem Value="p">Production</asp:ListItem>
</asp:DropDownList>
<br /><br />
<asp:Button ID="btnDBSelect" runat="server" Text="Next >>" OnClick="btnDBSelect_Click" />
