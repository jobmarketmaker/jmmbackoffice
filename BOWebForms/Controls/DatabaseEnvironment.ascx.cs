﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BOWebForms.Controls
{
    public partial class DatabaseEnvironment : System.Web.UI.UserControl
    {
        public event System.EventHandler DatabaseSelected;

        private JMMDatabase _jd;

        public JMMDatabase jd
        {
            get
            {
                JMMDatabase j = new JMMDatabase();
                switch (ddlDB.SelectedValue)
                {
                    case "d":;
                        j = JMMDatabase.DEV;
                        break;
                    case "s":
                        j = JMMDatabase.STAGING;
                        break;
                    case "x":
                        j = JMMDatabase.SANDBOX;
                        break;
                    case "p":
                        j = JMMDatabase.PRODUCTION;
                        break;
                }
                return j;
            }
            set { _jd = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDBSelect_Click(object sender, EventArgs e)
        {
            switch (ddlDB.SelectedValue)
            {
                case "d":
                    _jd = JMMDatabase.DEV;
                    break;
                case "s":
                    _jd = JMMDatabase.STAGING;
                    break;
                case "x":
                    _jd = JMMDatabase.SANDBOX;
                    break;
                case "p":
                    _jd = JMMDatabase.PRODUCTION;
                    break;
            }

            DatabaseSelected(this, new EventArgs()); // this event tells any parent controls (i.e. the .aspx pages containing this) that the button has been pressed
        }
    }
}