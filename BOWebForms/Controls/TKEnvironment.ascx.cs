﻿using BOWebForms.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BOWebForms.Controls
{
    public partial class TKEnvironment : System.Web.UI.UserControl
    {
        public event System.EventHandler TKSelected;

        private TKSystem _tk;

        public TKSystem tk
        {
            get
            {
                TKSystem t = new TKSystem();
                switch (ddlTK.SelectedValue)
                {
                    case "s":
                        t = TKSystem.STAGING;
                        break;
                    case "p":
                        t = TKSystem.PRODUCTION;
                        break;
                }
                return t;
            }
            set { _tk = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnTKSelect_Click(object sender, EventArgs e)
        {
            switch (ddlTK.SelectedValue)
            {
                case "s":
                    _tk = TKSystem.STAGING;
                    break;
                case "p":
                    _tk = TKSystem.PRODUCTION;
                    break;
            }

            TKSelected(this, new EventArgs()); // this event tells any parent controls (i.e. the .aspx pages containing this) that the button has been pressed
        }
    }
}