﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TKEnvironment.ascx.cs" Inherits="BOWebForms.Controls.TKEnvironment" %>

<h3>Select a TextKernel environment</h3>
<asp:DropDownList ID="ddlTK" runat="server">
    <asp:ListItem Selected="True" Value="s">Staging</asp:ListItem>
    <asp:ListItem Value="p">Production</asp:ListItem>
</asp:DropDownList>
<br /><br />
<asp:Button ID="btnTKSelect" runat="server" Text="Next >>" OnClick="btnTKSelect_Click" />
