﻿namespace JMMBackOffice
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.btnGoToUpdates = new System.Windows.Forms.Button();
            this.btnTKScorePrefiller = new System.Windows.Forms.Button();
            this.btnExportResume = new System.Windows.Forms.Button();
            this.btnUploadJobBoardImage = new System.Windows.Forms.Button();
            this.btnCheckTKCurrency = new System.Windows.Forms.Button();
            this.btnOneTimeScore = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQueryBackfiller = new System.Windows.Forms.Button();
            this.btnResetPassword = new System.Windows.Forms.Button();
            this.btnSoonToExpireJOs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(195, 272);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(153, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "(Original filler)";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(287, 240);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(61, 13);
            this.lblOutput.TabIndex = 1;
            this.lblOutput.Text = "[[lblOutput]]";
            this.lblOutput.Visible = false;
            // 
            // btnGoToUpdates
            // 
            this.btnGoToUpdates.Location = new System.Drawing.Point(194, 214);
            this.btnGoToUpdates.Name = "btnGoToUpdates";
            this.btnGoToUpdates.Size = new System.Drawing.Size(153, 23);
            this.btnGoToUpdates.TabIndex = 2;
            this.btnGoToUpdates.Text = "Updates";
            this.btnGoToUpdates.UseVisualStyleBackColor = true;
            this.btnGoToUpdates.Visible = false;
            this.btnGoToUpdates.Click += new System.EventHandler(this.btnGoToUpdates_Click);
            // 
            // btnTKScorePrefiller
            // 
            this.btnTKScorePrefiller.Location = new System.Drawing.Point(13, 243);
            this.btnTKScorePrefiller.Name = "btnTKScorePrefiller";
            this.btnTKScorePrefiller.Size = new System.Drawing.Size(153, 23);
            this.btnTKScorePrefiller.TabIndex = 3;
            this.btnTKScorePrefiller.Text = "TK Score Prefillerb";
            this.btnTKScorePrefiller.UseVisualStyleBackColor = true;
            this.btnTKScorePrefiller.Click += new System.EventHandler(this.btnTKScorePrefiller_Click);
            // 
            // btnExportResume
            // 
            this.btnExportResume.Location = new System.Drawing.Point(195, 12);
            this.btnExportResume.Name = "btnExportResume";
            this.btnExportResume.Size = new System.Drawing.Size(153, 23);
            this.btnExportResume.TabIndex = 4;
            this.btnExportResume.Text = "Export Resumes";
            this.btnExportResume.UseVisualStyleBackColor = true;
            this.btnExportResume.Click += new System.EventHandler(this.btnExportResume_Click);
            // 
            // btnUploadJobBoardImage
            // 
            this.btnUploadJobBoardImage.Location = new System.Drawing.Point(195, 42);
            this.btnUploadJobBoardImage.Name = "btnUploadJobBoardImage";
            this.btnUploadJobBoardImage.Size = new System.Drawing.Size(153, 23);
            this.btnUploadJobBoardImage.TabIndex = 5;
            this.btnUploadJobBoardImage.Text = "Upload a Job Board Image";
            this.btnUploadJobBoardImage.UseVisualStyleBackColor = true;
            this.btnUploadJobBoardImage.Click += new System.EventHandler(this.btnUploadJobBoardImage_Click);
            // 
            // btnCheckTKCurrency
            // 
            this.btnCheckTKCurrency.Location = new System.Drawing.Point(13, 12);
            this.btnCheckTKCurrency.Name = "btnCheckTKCurrency";
            this.btnCheckTKCurrency.Size = new System.Drawing.Size(153, 23);
            this.btnCheckTKCurrency.TabIndex = 6;
            this.btnCheckTKCurrency.Text = "Check TK Currency";
            this.btnCheckTKCurrency.UseVisualStyleBackColor = true;
            this.btnCheckTKCurrency.Click += new System.EventHandler(this.btnCheckTKCurrency_Click);
            // 
            // btnOneTimeScore
            // 
            this.btnOneTimeScore.Location = new System.Drawing.Point(13, 41);
            this.btnOneTimeScore.Name = "btnOneTimeScore";
            this.btnOneTimeScore.Size = new System.Drawing.Size(153, 23);
            this.btnOneTimeScore.TabIndex = 7;
            this.btnOneTimeScore.Text = "One-time C+JO TK Score";
            this.btnOneTimeScore.UseVisualStyleBackColor = true;
            this.btnOneTimeScore.Click += new System.EventHandler(this.btnOneTimeScore_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 2);
            this.label1.TabIndex = 8;
            this.label1.Text = "label1";
            // 
            // btnQueryBackfiller
            // 
            this.btnQueryBackfiller.Location = new System.Drawing.Point(12, 214);
            this.btnQueryBackfiller.Name = "btnQueryBackfiller";
            this.btnQueryBackfiller.Size = new System.Drawing.Size(153, 23);
            this.btnQueryBackfiller.TabIndex = 9;
            this.btnQueryBackfiller.Text = "Query Backfiller";
            this.btnQueryBackfiller.UseVisualStyleBackColor = true;
            this.btnQueryBackfiller.Click += new System.EventHandler(this.btnQueryBackfiller_Click);
            // 
            // btnResetPassword
            // 
            this.btnResetPassword.Location = new System.Drawing.Point(195, 71);
            this.btnResetPassword.Name = "btnResetPassword";
            this.btnResetPassword.Size = new System.Drawing.Size(153, 23);
            this.btnResetPassword.TabIndex = 10;
            this.btnResetPassword.Text = "Reset a Password";
            this.btnResetPassword.UseVisualStyleBackColor = true;
            this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
            // 
            // btnSoonToExpireJOs
            // 
            this.btnSoonToExpireJOs.Location = new System.Drawing.Point(13, 71);
            this.btnSoonToExpireJOs.Name = "btnSoonToExpireJOs";
            this.btnSoonToExpireJOs.Size = new System.Drawing.Size(153, 23);
            this.btnSoonToExpireJOs.TabIndex = 11;
            this.btnSoonToExpireJOs.Text = "See Soon-To-Expire JOs";
            this.btnSoonToExpireJOs.UseVisualStyleBackColor = true;
            this.btnSoonToExpireJOs.Click += new System.EventHandler(this.btnSoonToExpireJOs_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 309);
            this.Controls.Add(this.btnSoonToExpireJOs);
            this.Controls.Add(this.btnResetPassword);
            this.Controls.Add(this.btnQueryBackfiller);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOneTimeScore);
            this.Controls.Add(this.btnCheckTKCurrency);
            this.Controls.Add(this.btnUploadJobBoardImage);
            this.Controls.Add(this.btnExportResume);
            this.Controls.Add(this.btnTKScorePrefiller);
            this.Controls.Add(this.btnGoToUpdates);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.btnStart);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JMM Utilities";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Button btnGoToUpdates;
        private System.Windows.Forms.Button btnTKScorePrefiller;
        private System.Windows.Forms.Button btnExportResume;
        private System.Windows.Forms.Button btnUploadJobBoardImage;
        private System.Windows.Forms.Button btnCheckTKCurrency;
        private System.Windows.Forms.Button btnOneTimeScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQueryBackfiller;
        private System.Windows.Forms.Button btnResetPassword;
        private System.Windows.Forms.Button btnSoonToExpireJOs;
    }
}