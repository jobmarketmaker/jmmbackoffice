﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Xml;
using System.IO;

namespace JMMBackOffice
{
    public partial class frmCombosInPlay : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public frmCombosInPlay()
        {
            InitializeComponent();
        }

        private static XmlDocument CreateSoapEnvelopeForQuery(string s)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://home.textkernel.nl/sourcebox/soap/documentProcessor""><soapenv:Header/><soapenv:Body><doc:processDocumentAdvanced><account>jobmarketmaker_cv</account><username>query</username><password>8226bG5N</password><clientSpecificArguments><key>?</key><value>?</value></clientSpecificArguments><fileName>resume.doc</fileName><fileContent>" + s + "</fileContent></doc:processDocumentAdvanced></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private static XmlDocument CreateSoapEnvelopeForMatch(string candidate_Query, int job_Opening_id)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sear=""http://home.textkernel.nl/search""><soapenv:Header/><soapenv:Body><sear:search><environment>jobmarketmaker_vac</environment><password>qGVWDu6p</password><accessRoles>all</accessRoles><request><query>" + candidate_Query + " jmm_opening_id:" + job_Opening_id.ToString() + "</query><resultOffset>0</resultOffset><searchEngine>jobmarketmaker_vac</searchEngine><inputLanguage>en</inputLanguage><outputLanguage>en</outputLanguage></request></sear:search></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private void btnFillGrid_Click(object sender, EventArgs e)
        {
            BindGrid();
            btnGenerateQueries.Enabled = true;
        }

        private void BindGrid()
        {
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_SelCombosInPlayb", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);

            dgv.DataSource = dt;

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void btnGenerateQueries_Click(object sender, EventArgs e)
        {
            int combos_in_play_Id = 0;
            int candidate_Id = 0;
            int job_opening_Id = 0;
            string candidate_Query = "";
            decimal tk_Score = 0.0M;

            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                try
                {
                    combos_in_play_Id = int.Parse(dgvr.Cells[0].Value.ToString());
                    candidate_Id = int.Parse(dgvr.Cells[1].Value.ToString());
                    job_opening_Id = int.Parse(dgvr.Cells[2].Value.ToString());


                    ////-------------- first, get the query -------------------//
                    //candidate_Query = GetCandidateQuery(candidate_Id).Replace("&", "&amp;");

                    //dgvr.Cells[3].Value = candidate_Query;
                    //Application.DoEvents();

                    //SaveCandidateQuery(candidate_Id, candidate_Query);
                    candidate_Query = dgvr.Cells[3].Value.ToString();

                    //-------------- now, do the score -------------------//
                    tk_Score = GetTKScore(candidate_Query, job_opening_Id);

                    dgvr.Cells[4].Value = tk_Score.ToString();
                    Application.DoEvents();

                    SaveTKScore(combos_in_play_Id, tk_Score);
                }
                catch (Exception ex)
                {
                    txtErrors.Text += ex.Message + "\n";
                }
            }
        }

        private decimal GetTKScore(string candidate_Query, int job_Opening_Id)
        {
            decimal retVal = 0.0M;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match-SearchBox/soap/search");
            XmlDocument soapEnvelopXml = CreateSoapEnvelopeForMatch(candidate_Query, job_Opening_Id);

            request.Method = "POST";
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(temp);
            XmlNodeList xnl = returnDoc.GetElementsByTagName("score");

            if (xnl.Count == 0)
            {
                retVal = 0.0M;
            }
            else
            {
                retVal = decimal.Parse(xnl.Item(0).InnerText);
            }

            return retVal;
        }

        private string GetCandidateQuery(int candidate_Id)
        {
            string retVal = "";

            css = settings["JMM-Nov6Beta"];

            // retrieve the Candidate resume from the db
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelCombosInPlay_CandidateResume";
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
            conn.Open();

            byte[] resume = (byte[])cmd.ExecuteScalar();
            cmd = null;
            conn.Close();
            conn = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match/soap/documentProcessor"); // ("https://staging.textkernel.nl:443/match/soap/documentProcessor")
            string resumeBase64 = Convert.ToBase64String(resume);
            XmlDocument soapEnvelopXml = CreateSoapEnvelopeForQuery(resumeBase64);

            request.Method = "POST";
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }

            XmlDocument justTheReturnNode = new XmlDocument();
            justTheReturnNode.LoadXml(temp);
            XmlNodeList xnl = justTheReturnNode.GetElementsByTagName("return");

            retVal = xnl.Item(0).InnerText;

            return retVal;
        }

        private void SaveCandidateQuery(int candidate_Id, string candidate_Query)
        {
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_UpdCombosInPlayb_TKQuery";
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
            cmd.Parameters.Add(new SqlParameter("TK_Query", candidate_Query));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void SaveTKScore(int combos_In_Play_Id, decimal tk_Score)
        {
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_UpdCombosInPlayb_TKScore";
            cmd.Parameters.Add(new SqlParameter("Combos_In_Playb_Id", combos_In_Play_Id));
            cmd.Parameters.Add(new SqlParameter("TK_Score", tk_Score));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }
    }
}
