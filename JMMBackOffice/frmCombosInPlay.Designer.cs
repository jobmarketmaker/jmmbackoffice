﻿namespace JMMBackOffice
{
    partial class frmCombosInPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFillGrid = new System.Windows.Forms.Button();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnGenerateQueries = new System.Windows.Forms.Button();
            this.txtErrors = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFillGrid
            // 
            this.btnFillGrid.Location = new System.Drawing.Point(12, 12);
            this.btnFillGrid.Name = "btnFillGrid";
            this.btnFillGrid.Size = new System.Drawing.Size(200, 23);
            this.btnFillGrid.TabIndex = 2;
            this.btnFillGrid.Text = "Fill grid";
            this.btnFillGrid.UseVisualStyleBackColor = true;
            this.btnFillGrid.Click += new System.EventHandler(this.btnFillGrid_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 41);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(811, 353);
            this.dgv.TabIndex = 3;
            // 
            // btnGenerateQueries
            // 
            this.btnGenerateQueries.Enabled = false;
            this.btnGenerateQueries.Location = new System.Drawing.Point(218, 12);
            this.btnGenerateQueries.Name = "btnGenerateQueries";
            this.btnGenerateQueries.Size = new System.Drawing.Size(200, 23);
            this.btnGenerateQueries.TabIndex = 4;
            this.btnGenerateQueries.Text = "Generate Queries";
            this.btnGenerateQueries.UseVisualStyleBackColor = true;
            this.btnGenerateQueries.Click += new System.EventHandler(this.btnGenerateQueries_Click);
            // 
            // txtErrors
            // 
            this.txtErrors.Location = new System.Drawing.Point(12, 400);
            this.txtErrors.Multiline = true;
            this.txtErrors.Name = "txtErrors";
            this.txtErrors.Size = new System.Drawing.Size(811, 75);
            this.txtErrors.TabIndex = 5;
            // 
            // frmCombosInPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 487);
            this.Controls.Add(this.txtErrors);
            this.Controls.Add(this.btnGenerateQueries);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.btnFillGrid);
            this.Name = "frmCombosInPlay";
            this.Text = "Combos In Play";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFillGrid;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnGenerateQueries;
        private System.Windows.Forms.TextBox txtErrors;
    }
}