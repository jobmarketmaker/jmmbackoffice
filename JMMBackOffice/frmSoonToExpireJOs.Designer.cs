﻿namespace JMMBackOffice
{
    partial class frmSoonToExpireJOs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnFillGrid = new System.Windows.Forms.Button();
            this.txtReportFolder = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnExportReport = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.nudDaysOut = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDaysOut)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 41);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(811, 353);
            this.dgv.TabIndex = 5;
            // 
            // btnFillGrid
            // 
            this.btnFillGrid.Location = new System.Drawing.Point(12, 12);
            this.btnFillGrid.Name = "btnFillGrid";
            this.btnFillGrid.Size = new System.Drawing.Size(200, 23);
            this.btnFillGrid.TabIndex = 4;
            this.btnFillGrid.Text = "Fill grid";
            this.btnFillGrid.UseVisualStyleBackColor = true;
            this.btnFillGrid.Click += new System.EventHandler(this.btnFillGrid_Click);
            // 
            // txtReportFolder
            // 
            this.txtReportFolder.Location = new System.Drawing.Point(545, 14);
            this.txtReportFolder.Name = "txtReportFolder";
            this.txtReportFolder.Size = new System.Drawing.Size(197, 20);
            this.txtReportFolder.TabIndex = 6;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(748, 12);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFolder.TabIndex = 7;
            this.btnSelectFolder.Text = "Select folder";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnExportReport
            // 
            this.btnExportReport.Location = new System.Drawing.Point(309, 14);
            this.btnExportReport.Name = "btnExportReport";
            this.btnExportReport.Size = new System.Drawing.Size(230, 21);
            this.btnExportReport.TabIndex = 8;
            this.btnExportReport.Text = "Export Report";
            this.btnExportReport.UseVisualStyleBackColor = true;
            this.btnExportReport.Click += new System.EventHandler(this.btnExportReport_Click);
            // 
            // nudDaysOut
            // 
            this.nudDaysOut.Location = new System.Drawing.Point(219, 14);
            this.nudDaysOut.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.nudDaysOut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDaysOut.Name = "nudDaysOut";
            this.nudDaysOut.Size = new System.Drawing.Size(65, 20);
            this.nudDaysOut.TabIndex = 9;
            this.nudDaysOut.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // frmSoonToExpireJOs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 416);
            this.Controls.Add(this.nudDaysOut);
            this.Controls.Add(this.btnExportReport);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.txtReportFolder);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.btnFillGrid);
            this.Name = "frmSoonToExpireJOs";
            this.Text = "Soon-To-Expire Job Openings";
            this.Load += new System.EventHandler(this.frmSoonToExpireJOs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDaysOut)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnFillGrid;
        private System.Windows.Forms.TextBox txtReportFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnExportReport;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.NumericUpDown nudDaysOut;
    }
}