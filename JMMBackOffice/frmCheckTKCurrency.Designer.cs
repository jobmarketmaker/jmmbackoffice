﻿namespace JMMBackOffice
{
    partial class frmCheckTKCurrency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheckTK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumberOfJobOpenings = new System.Windows.Forms.Label();
            this.txtJobOpeningIds = new System.Windows.Forms.TextBox();
            this.btnCheckJMM = new System.Windows.Forms.Button();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnGenerateXMLDocs = new System.Windows.Forms.Button();
            this.txtDestinationFolder = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnSendUpdatesToTK = new System.Windows.Forms.Button();
            this.btnMarkAsUpdated = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCheckTK
            // 
            this.btnCheckTK.Location = new System.Drawing.Point(13, 13);
            this.btnCheckTK.Name = "btnCheckTK";
            this.btnCheckTK.Size = new System.Drawing.Size(200, 23);
            this.btnCheckTK.TabIndex = 0;
            this.btnCheckTK.Text = "Check TK";
            this.btnCheckTK.UseVisualStyleBackColor = true;
            this.btnCheckTK.Click += new System.EventHandler(this.btnCheckTK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of Job Openings:";
            // 
            // lblNumberOfJobOpenings
            // 
            this.lblNumberOfJobOpenings.AutoSize = true;
            this.lblNumberOfJobOpenings.Location = new System.Drawing.Point(197, 43);
            this.lblNumberOfJobOpenings.Name = "lblNumberOfJobOpenings";
            this.lblNumberOfJobOpenings.Size = new System.Drawing.Size(16, 13);
            this.lblNumberOfJobOpenings.TabIndex = 2;
            this.lblNumberOfJobOpenings.Text = "---";
            this.lblNumberOfJobOpenings.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtJobOpeningIds
            // 
            this.txtJobOpeningIds.Location = new System.Drawing.Point(16, 60);
            this.txtJobOpeningIds.Multiline = true;
            this.txtJobOpeningIds.Name = "txtJobOpeningIds";
            this.txtJobOpeningIds.Size = new System.Drawing.Size(197, 317);
            this.txtJobOpeningIds.TabIndex = 3;
            // 
            // btnCheckJMM
            // 
            this.btnCheckJMM.Location = new System.Drawing.Point(219, 13);
            this.btnCheckJMM.Name = "btnCheckJMM";
            this.btnCheckJMM.Size = new System.Drawing.Size(200, 23);
            this.btnCheckJMM.TabIndex = 4;
            this.btnCheckJMM.Text = "Check JMM";
            this.btnCheckJMM.UseVisualStyleBackColor = true;
            this.btnCheckJMM.Click += new System.EventHandler(this.btnCheckJMM_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(219, 60);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(824, 317);
            this.dgv.TabIndex = 6;
            // 
            // btnGenerateXMLDocs
            // 
            this.btnGenerateXMLDocs.Location = new System.Drawing.Point(1049, 60);
            this.btnGenerateXMLDocs.Name = "btnGenerateXMLDocs";
            this.btnGenerateXMLDocs.Size = new System.Drawing.Size(152, 23);
            this.btnGenerateXMLDocs.TabIndex = 7;
            this.btnGenerateXMLDocs.Text = "Generate missing XML docs";
            this.btnGenerateXMLDocs.UseVisualStyleBackColor = true;
            this.btnGenerateXMLDocs.Click += new System.EventHandler(this.btnGenerateXMLDocs_Click);
            // 
            // txtDestinationFolder
            // 
            this.txtDestinationFolder.Location = new System.Drawing.Point(1049, 89);
            this.txtDestinationFolder.Name = "txtDestinationFolder";
            this.txtDestinationFolder.Size = new System.Drawing.Size(64, 20);
            this.txtDestinationFolder.TabIndex = 8;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(1119, 87);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(82, 23);
            this.btnSelectFolder.TabIndex = 9;
            this.btnSelectFolder.Text = "Select folder";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnSendUpdatesToTK
            // 
            this.btnSendUpdatesToTK.Location = new System.Drawing.Point(1049, 137);
            this.btnSendUpdatesToTK.Name = "btnSendUpdatesToTK";
            this.btnSendUpdatesToTK.Size = new System.Drawing.Size(152, 23);
            this.btnSendUpdatesToTK.TabIndex = 10;
            this.btnSendUpdatesToTK.Text = "Send updates to TK";
            this.btnSendUpdatesToTK.UseVisualStyleBackColor = true;
            this.btnSendUpdatesToTK.Click += new System.EventHandler(this.btnSendUpdatesToTK_Click);
            // 
            // btnMarkAsUpdated
            // 
            this.btnMarkAsUpdated.Location = new System.Drawing.Point(1049, 197);
            this.btnMarkAsUpdated.Name = "btnMarkAsUpdated";
            this.btnMarkAsUpdated.Size = new System.Drawing.Size(152, 23);
            this.btnMarkAsUpdated.TabIndex = 11;
            this.btnMarkAsUpdated.Text = "Mark as updated";
            this.btnMarkAsUpdated.UseVisualStyleBackColor = true;
            this.btnMarkAsUpdated.Click += new System.EventHandler(this.btnMarkAsUpdated_Click);
            // 
            // frmCheckTKCurrency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 389);
            this.Controls.Add(this.btnMarkAsUpdated);
            this.Controls.Add(this.btnSendUpdatesToTK);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.txtDestinationFolder);
            this.Controls.Add(this.btnGenerateXMLDocs);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.btnCheckJMM);
            this.Controls.Add(this.txtJobOpeningIds);
            this.Controls.Add(this.lblNumberOfJobOpenings);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCheckTK);
            this.Name = "frmCheckTKCurrency";
            this.Text = "Check TK Currency";
            this.Load += new System.EventHandler(this.frmCheckTKCurrency_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheckTK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNumberOfJobOpenings;
        private System.Windows.Forms.TextBox txtJobOpeningIds;
        private System.Windows.Forms.Button btnCheckJMM;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnGenerateXMLDocs;
        private System.Windows.Forms.TextBox txtDestinationFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnSendUpdatesToTK;
        private System.Windows.Forms.Button btnMarkAsUpdated;
    }
}