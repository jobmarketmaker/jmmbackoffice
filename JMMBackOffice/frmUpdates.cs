﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace JMMBackOffice
{
    public partial class frmUpdates : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public frmUpdates()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblHeader.Enabled = true;

            BindGrid();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            CheckBox cb = new CheckBox();
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                DataGridViewCheckBoxCell dgvcbc = new DataGridViewCheckBoxCell();
                dgvcbc = (DataGridViewCheckBoxCell)dgvr.Cells[3];
                if (dgvcbc.Value != null && dgvcbc.Value.ToString().ToLower() == "true")
                {
                    SqlConnection conn = new SqlConnection(css.ConnectionString);
                    SqlCommand cmd = new SqlCommand("", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "up_UpdUploadValue";
                    cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", dgvr.Cells[0].Value.ToString()));
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    cmd = null;
                    conn.Close();
                    conn = null;
                }
            }
            BindGrid();
        }

        private void BindGrid()
        {
            css = settings["JMM-Nov6Beta"];
            //css = settings["JMM-Dev"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelJobModif";
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);

            dgv.DataSource = dt;
            dgv.Columns[3].HeaderText = "Mark as uploaded?";
            dgv.Columns[0].Width = (dgv.DisplayRectangle.Width / 4) - 15;
            dgv.Columns[1].Width = (dgv.DisplayRectangle.Width / 4) - 15;
            dgv.Columns[2].Width = (dgv.DisplayRectangle.Width / 4) - 15;
            dgv.Columns[3].Width = (dgv.DisplayRectangle.Width / 4) - 15;

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void btnTestEmail_Click(object sender, EventArgs e)
        {
            var fromAddress = new MailAddress("hello@jobmarketmaker.com", "hello support");
            var toAddress = new MailAddress("chris.valdivia@jobmarketmaker.com", "Chris Valdivia");
            var subject = "Job Openings have been changed";
            var body = "The following Job Openings have been modified and require attention:";
            var smtp = new SmtpClient
            {
                Host = "smtp.office365.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("hello@jobmarketmaker.com", "ButterBall$")
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}
