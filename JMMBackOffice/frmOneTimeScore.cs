﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.Xml;
using System.IO;

namespace JMMBackOffice
{
    public partial class frmOneTimeScore : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public frmOneTimeScore()
        {
            InitializeComponent();
        }

        private static XmlDocument CreateSoapEnvelopeForQuery(string s)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://home.textkernel.nl/sourcebox/soap/documentProcessor""><soapenv:Header/><soapenv:Body><doc:processDocumentAdvanced><account>jobmarketmaker_cv</account><username>query</username><password>8226bG5N</password><clientSpecificArguments><key>?</key><value>?</value></clientSpecificArguments><fileName>resume.doc</fileName><fileContent>" + s + "</fileContent></doc:processDocumentAdvanced></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private static XmlDocument CreateSoapEnvelopeForMatch(string candidate_Query, int job_Opening_id)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            //string queryToPassIn = candidate_Query;
            candidate_Query = candidate_Query.Replace("&amp;", "xxxrestorexxx");
            candidate_Query = candidate_Query.Replace("&", "&amp;");
            candidate_Query = candidate_Query.Replace("xxxrestorexxx", "&amp;");

            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sear=""http://home.textkernel.nl/search""><soapenv:Header/><soapenv:Body><sear:search><environment>jobmarketmaker_vac</environment><password>qGVWDu6p</password><accessRoles>all</accessRoles><request><query>" + candidate_Query + " jmm_opening_id:" + job_Opening_id.ToString() + "</query><resultOffset>0</resultOffset><searchEngine>jobmarketmaker_vac</searchEngine><inputLanguage>en</inputLanguage><outputLanguage>en</outputLanguage></request></sear:search></soapenv:Body></soapenv:Envelope>");
            //soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sear=""http://home.textkernel.nl/search""><soapenv:Header/><soapenv:Body><sear:search><environment>jobmarketmaker_vac</environment><password>qGVWDu6p</password><accessRoles>all</accessRoles><request><query>" + queryToPassIn + " jmm_opening_id:" + job_Opening_id.ToString() + "</query><resultOffset>0</resultOffset><searchEngine>jobmarketmaker_vac</searchEngine><inputLanguage>en</inputLanguage><outputLanguage>en</outputLanguage></request></sear:search></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private decimal GetTKScore(string candidate_Query, int job_Opening_Id)
        {
            decimal retVal = 0.0M;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match-SearchBox/soap/search");
            XmlDocument soapEnvelopXml = CreateSoapEnvelopeForMatch(candidate_Query, job_Opening_Id);

            request.Method = "POST";
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(temp);
            XmlNodeList xnl = returnDoc.GetElementsByTagName("score");

            if (xnl.Count == 0)
            {
                retVal = 0.0M;
            }
            else
            {
                retVal = decimal.Parse(xnl.Item(0).InnerText);
            }

            return retVal;
        }

        private string GetCandidateQuery(int candidate_Id)
        {
            string retVal = "";

            css = settings["JMM-Nov6Beta"];
            //css = settings["JMM-Staging"];

            // retrieve the Candidate resume from the db
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelCombosInPlay_CandidateResume";
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
            conn.Open();

            byte[] resume = (byte[])cmd.ExecuteScalar();
            cmd = null;
            conn.Close();
            conn = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match/soap/documentProcessor");
            string resumeBase64 = Convert.ToBase64String(resume);
            XmlDocument soapEnvelopXml = CreateSoapEnvelopeForQuery(resumeBase64);

            request.Method = "POST";
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }

            XmlDocument justTheReturnNode = new XmlDocument();
            justTheReturnNode.LoadXml(temp);
            XmlNodeList xnl = justTheReturnNode.GetElementsByTagName("return");

            retVal = xnl.Item(0).InnerText;

            return retVal;
        }

        private void btnGenerateScore_Click(object sender, EventArgs e)
        {
            int candidate_Id = int.Parse(txtCandidateId.Text);
            int job_opening_Id = int.Parse(txtJobOpeningId.Text);

            string candidate_Query = "";
            decimal tk_Score = 0.0M;

            candidate_Query = GetCandidateQuery(candidate_Id);
            txtGeneratedQuery.Text = candidate_Query;

            tk_Score = GetTKScore(candidate_Query, job_opening_Id);
            txtGeneratedScore.Text = tk_Score.ToString();

            txtSQLToExecute.Text = "UPDATE xxxSOMETABLExxx SET xxxSOMEFIELDxxx = " + tk_Score + " WHERE Candidate_ID = " + candidate_Id.ToString() + " AND Job_Opening_Id = " + job_opening_Id.ToString();
        }
    }
}
