﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JMMBackOffice
{
    public partial class frmSoonToExpireJOs : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();
        const string DEFAULT_FOLDER = "c:\\MyExcelDump";
        DataTable dt = new DataTable();

        public frmSoonToExpireJOs()
        {
            InitializeComponent();
        }

        private void btnFillGrid_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            //if (dgv.Rows.Count > 0)
            //    dgv.Rows.Clear();

            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_Reporting_JobOpenings_SoonToExpire", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("DaysNoticeThreshold", (int)nudDaysOut.Value));
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            dgv.DataSource = dt;

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void frmSoonToExpireJOs_Load(object sender, EventArgs e)
        {
            txtReportFolder.Text = DEFAULT_FOLDER;
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = DEFAULT_FOLDER;
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtReportFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnExportReport_Click(object sender, EventArgs e)
        {
            // thank you http://stackoverflow.com/questions/4959722/c-sharp-datatable-to-csv
            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                  string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(DEFAULT_FOLDER + "\\soontoexpire_" + System.DateTime.Now.ToString("yyyyMMdd") + "_" + nudDaysOut.Value.ToString() + "days" + ".csv", sb.ToString());
        }
    }
}
