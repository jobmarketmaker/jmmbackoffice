﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace JMMBackOffice
{
    public partial class frmCheckTKCurrency : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();
        const string DEFAULT_FOLDER = "c:\\MyTKXML";

        public frmCheckTKCurrency()
        {
            InitializeComponent();
        }

        private void btnCheckTK_Click(object sender, EventArgs e)
        {
            txtJobOpeningIds.Text = "";

            bool matchAgain = true;
            int ro = 0;

            while (matchAgain)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match-SearchBox/soap/search");
                request.Method = "POST";
                request.ContentType = "text/xml";
                XmlDocument soapEnvelopXml = new XmlDocument();
                soapEnvelopXml = CreateDummySoapEnvelope(ro);
                soapEnvelopXml.Save(request.GetRequestStream());

                string temp = "";
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    temp = reader.ReadToEnd();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                XmlDocument returnDoc = new XmlDocument();
                returnDoc.LoadXml(temp);

                if (ro == 0)
                {
                    int recordCount = 0;
                    XmlNodeList xnl = returnDoc.GetElementsByTagName("matchSize");
                    if (xnl.Count == 0)
                        recordCount = 0;
                    else
                        recordCount = int.Parse(xnl.Item(0).InnerText);
                    lblNumberOfJobOpenings.Text = recordCount.ToString();
                    Application.DoEvents();
                }

                XmlNodeList resultItemsNodes = returnDoc.GetElementsByTagName("resultItems");
                if (resultItemsNodes.Count == 0)
                    matchAgain = false;
                else
                {
                    ro += 20;
                    foreach (XmlNode rin in resultItemsNodes)
                    {
                        var fields = rin["fields"];
                        var item = fields["item"];
                        var job_opening_id = item.InnerText;
                        txtJobOpeningIds.Text += job_opening_id.ToString() + ",";
                    }
                }
                Application.DoEvents();
            }
        }

        private static XmlDocument CreateDummySoapEnvelope(int resultOffset)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:sear=""http://home.textkernel.nl/search""><soapenv:Header/><soapenv:Body><sear:search><environment>jobmarketmaker_vac</environment><password>qGVWDu6p</password><accessRoles>all</accessRoles><request><query></query><resultOffset>" + resultOffset.ToString() + "</resultOffset><searchEngine>jobmarketmaker_vac</searchEngine><inputLanguage>en</inputLanguage><outputLanguage>en</outputLanguage></request></sear:search></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private void btnCheckJMM_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            // grab JOIds from the database
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_Reporting_JobOpenings";
            cmd.Parameters.Add(new SqlParameter("Status", ""));
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DataView dv = dt.DefaultView;
            dv.Sort = "Job_Opening_Id";

            //dgv.AutoGenerateColumns = false;
            dgv.DataSource = dv;

            foreach (DataGridViewColumn dgvc in dgv.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.NotSortable;
                if (dgvc.Name != "Job_Opening_Id" && dgvc.Name != "Last_Modified_Date" && dgvc.Name != "Employer_Name" && dgvc.Name != "Job_Title")
                    dgvc.Visible = false;
                else
                    dgvc.Width = 150;
            }

            cmd = null;
            conn.Close();
            conn = null;

            // Loop through our database records. Is TK missing anything?
            int job_opening_id;
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                job_opening_id = int.Parse(dgvr.Cells["Job_Opening_Id"].Value.ToString());
                if (txtJobOpeningIds.Text.Contains(job_opening_id.ToString()))
                {
                    dgvr.Cells["Job_Opening_Id"].Style.BackColor = Color.LightGreen;
                    dgvr.Cells["Last_Modified_Date"].Style.BackColor = Color.LightGreen;
                    dgvr.Cells["Employer_Name"].Style.BackColor = Color.LightGreen;
                    dgvr.Cells["Job_Title"].Style.BackColor = Color.LightGreen;
                }
                else
                {
                    dgvr.Cells["Job_Opening_Id"].Style.BackColor = Color.Pink;
                    dgvr.Cells["Last_Modified_Date"].Style.BackColor = Color.Pink;
                    dgvr.Cells["Employer_Name"].Style.BackColor = Color.Pink;
                    dgvr.Cells["Job_Title"].Style.BackColor = Color.Pink;
                }
            }
        }

        private void btnGenerateXMLDocs_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgvsrc = dgv.SelectedRows;

            css = settings["JMM-Nov6Beta"];
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_GenerateJobOpeningXMLForTK";

            int job_opening_id;
            string TKXML;

            conn.Open();

            foreach (DataGridViewRow dgvr in dgvsrc)
            {
                job_opening_id = int.Parse(dgvr.Cells["Job_Opening_Id"].Value.ToString());
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", job_opening_id));

                SqlDataReader dr = cmd.ExecuteReader();
                
                System.IO.StreamWriter file;
                while (dr.Read())
                {
                    TKXML = dr["TKXML"].ToString();

                    file = new StreamWriter(txtDestinationFolder.Text + "\\" + job_opening_id.ToString() + ".xml");
                    file.WriteLine(TKXML);

                    file.Close();
                }

                dr.Close();
            }

            cmd = null;
            conn.Close();
            conn = null;

            RefreshGrid();
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = DEFAULT_FOLDER;
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtDestinationFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void frmCheckTKCurrency_Load(object sender, EventArgs e)
        {
            txtDestinationFolder.Text = DEFAULT_FOLDER;
        }

        private void btnMarkAsUpdated_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgvsrc = dgv.SelectedRows;

            css = settings["JMM-Nov6Beta"];
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_UpdUploadValue";

            int job_opening_id;

            conn.Open();

            foreach (DataGridViewRow dgvr in dgvsrc)
            {
                job_opening_id = int.Parse(dgvr.Cells["Job_Opening_Id"].Value.ToString());
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", job_opening_id));
                int dummy = cmd.ExecuteNonQuery();
            }

            cmd = null;
            conn.Close();
            conn = null;

            RefreshGrid();
        }

        private void btnSendUpdatesToTK_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgvsrc = dgv.SelectedRows;

            css = settings["JMM-Nov6Beta"];
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_GenerateJobOpeningXMLForTK";

            int job_opening_id;
            string TKXML = "";

            conn.Open();

            foreach (DataGridViewRow dgvr in dgvsrc)
            {
                job_opening_id = int.Parse(dgvr.Cells["Job_Opening_Id"].Value.ToString());
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("Job_Opening_Id", job_opening_id));
                SqlDataReader dr = cmd.ExecuteReader();
                
                while (dr.Read())
                {
                    TKXML = dr["TKXML"].ToString();
                }

                dr.Close();

                TKXML = TKXML.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "");
                TKXML = TKXML.Replace("\r\n<Vacancy", "<Vacancy");
                byte[] b = Encoding.UTF8.GetBytes(TKXML);
                string encodedTKXML = Convert.ToBase64String(b);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match/soap/documentProcessor"); // ("https://home.textkernel.nl:443/match-SearchBox/soap/search");
                request.Method = "POST";
                request.ContentType = "text/xml";
                XmlDocument soapEnvelopXml = new XmlDocument();

                soapEnvelopXml = CreateVacancySoapEnvelope(encodedTKXML, job_opening_id); // (TKXML);
                soapEnvelopXml.Save(request.GetRequestStream());

                string temp = "";
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    temp = reader.ReadToEnd();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                XmlDocument returnDoc = new XmlDocument();
                returnDoc.LoadXml(temp);

                // soooo.... do we need to do anything with returnDoc? Probably not...
            }

            cmd = null;
            conn.Close();
            conn = null;

            RefreshGrid();
        }

        private static XmlDocument CreateVacancySoapEnvelope(string TKXML, int job_opening_id)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://home.textkernel.nl/sourcebox/soap/documentProcessor""><soapenv:Header/><soapenv:Body><doc:processDocument><account>jobmarketmaker_vac</account><username>jobmarketmaker</username><password>ierl9564</password><fileName>" + job_opening_id.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmtt") + ".xml</fileName><fileContent>" + TKXML + "</fileContent></doc:processDocument></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }
    }
}
