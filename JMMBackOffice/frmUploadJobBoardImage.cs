﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace JMMBackOffice
{
    public partial class frmUploadJobBoardImage : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();
        const string DEFAULT_FOLDER = "c:\\MyLogo";

        public frmUploadJobBoardImage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = DEFAULT_FOLDER;
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtImageLocation.Text = openFileDialog1.FileName;
            }
        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            int job_Board_Id = 0;

            try
            {
                job_Board_Id = int.Parse(txtJobBoardId.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please enter a valid Job_Board_Id");
                return;
            }

            byte[] logo = File.ReadAllBytes(txtImageLocation.Text);

            css = settings["JMM-Nov6Beta"]; //["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_UpdJobBoard_Logo";
            cmd.Parameters.Add(new SqlParameter("Job_Board_Id", job_Board_Id));
            cmd.Parameters.Add(new SqlParameter("Logo", logo));
            conn.Open();

            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }
    }
}
