﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace JMMBackOffice
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            lblOutput.Text = "";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo("c:\\TKXMLFiles");
            foreach (System.IO.FileInfo fi in di.GetFiles())
            {
                fi.Delete();
            }

            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
            ConnectionStringSettings css = new ConnectionStringSettings();
            css = settings["JMM-Nov6Beta"];
            //MessageBox.Show(css.ConnectionString);

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_GenerateJobOpeningXMLForTK";
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            int Job_Opening_Id;
            string status;
            string TKXML;
            string Job_Title;
            string Employer_Name;
            string fileNamePrefix;

            System.IO.StreamWriter file;

            int iFileCount = 0;

            while (dr.Read())
            {
                Job_Opening_Id = int.Parse(dr["Job_Opening_Id"].ToString());
                status = dr["Status"].ToString();
                TKXML = dr["TKXML"].ToString();
                Job_Title = dr["Job_Title"].ToString();
                Employer_Name = dr["Employer_Name"].ToString();

                fileNamePrefix = status == "" ? "_" : "";

                //file = new System.IO.StreamWriter("c:\\TKXMLFiles\\" + Job_Opening_Id.ToString() + "_" + Job_Title.Replace(" ", "").Replace("\\", "").Replace("/", "") + "_" + Employer_Name.Replace(" ", "").Replace("\\", "").Replace("/", "") + ".xml");
                file = new System.IO.StreamWriter("c:\\Users\\Chris\\Dropbox\\_JMM\\TextKernel\\TKXMLWithJOsProper\\" + fileNamePrefix + Job_Opening_Id.ToString() + "_" + Job_Title.Replace(" ", "").Replace("\\", "").Replace("/", "") + "_" + Employer_Name.Replace(" ", "").Replace("\\", "").Replace("/", "") + ".xml");
                file.WriteLine(TKXML);

                file.Close();
                iFileCount++;
            }
            cmd = null;
            conn.Close();
            conn = null;

            lblOutput.Text = "Files processed: " + iFileCount.ToString();
        }

        private void btnGoToUpdates_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmUpdates"];
            if (frmTest == null)
            {
                frmUpdates f = new frmUpdates();
                f.Show();
            }
        }

        private void btnTKScorePrefiller_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmCombosInPlay"];
            if (frmTest == null)
            {
                frmCombosInPlay f = new frmCombosInPlay();
                f.Show();
            }
        }

        private void btnExportResume_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmExportResume"];
            if (frmTest == null)
            {
                frmExportResume f = new frmExportResume();
                f.Show();
            }
        }

        private void btnUploadJobBoardImage_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmUploadJobBoardImage"];
            if (frmTest == null)
            {
                frmUploadJobBoardImage f = new frmUploadJobBoardImage();
                f.Show();
            }
        }

        private void btnCheckTKCurrency_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmCheckTKCurrency"];
            if (frmTest == null)
            {
                frmCheckTKCurrency f = new frmCheckTKCurrency();
                f.Show();
            }
        }

        private void btnOneTimeScore_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmOneTimeScore"];
            if (frmTest == null)
            {
                frmOneTimeScore f = new frmOneTimeScore();
                f.Show();
            }
        }

        private void btnQueryBackfiller_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmQueryBackfiller"];
            if (frmTest == null)
            {
                frmQueryBackfiller f = new frmQueryBackfiller();
                f.Show();
            }
        }

        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon.....");
        }

        private void btnSoonToExpireJOs_Click(object sender, EventArgs e)
        {
            Form frmTest = Application.OpenForms["frmSoonToExpireJOs"];
            if (frmTest == null)
            {
                frmSoonToExpireJOs f = new frmSoonToExpireJOs();
                f.Show();
            }
        }
    }
}
