﻿namespace JMMBackOffice
{
    partial class frmOneTimeScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCandidateId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtJobOpeningId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGenerateScore = new System.Windows.Forms.Button();
            this.txtGeneratedQuery = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGeneratedScore = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSQLToExecute = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCandidateId
            // 
            this.txtCandidateId.Location = new System.Drawing.Point(154, 6);
            this.txtCandidateId.MaxLength = 6;
            this.txtCandidateId.Name = "txtCandidateId";
            this.txtCandidateId.Size = new System.Drawing.Size(100, 20);
            this.txtCandidateId.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Candidate_Id:";
            // 
            // txtJobOpeningId
            // 
            this.txtJobOpeningId.Location = new System.Drawing.Point(154, 32);
            this.txtJobOpeningId.MaxLength = 6;
            this.txtJobOpeningId.Name = "txtJobOpeningId";
            this.txtJobOpeningId.Size = new System.Drawing.Size(100, 20);
            this.txtJobOpeningId.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Job_Opening_Id:";
            // 
            // btnGenerateScore
            // 
            this.btnGenerateScore.Location = new System.Drawing.Point(220, 82);
            this.btnGenerateScore.Name = "btnGenerateScore";
            this.btnGenerateScore.Size = new System.Drawing.Size(230, 40);
            this.btnGenerateScore.TabIndex = 6;
            this.btnGenerateScore.Text = "Generate Score";
            this.btnGenerateScore.UseVisualStyleBackColor = true;
            this.btnGenerateScore.Click += new System.EventHandler(this.btnGenerateScore_Click);
            // 
            // txtGeneratedQuery
            // 
            this.txtGeneratedQuery.Location = new System.Drawing.Point(154, 135);
            this.txtGeneratedQuery.Name = "txtGeneratedQuery";
            this.txtGeneratedQuery.Size = new System.Drawing.Size(483, 20);
            this.txtGeneratedQuery.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Generated Query:";
            // 
            // txtGeneratedScore
            // 
            this.txtGeneratedScore.Location = new System.Drawing.Point(154, 161);
            this.txtGeneratedScore.Name = "txtGeneratedScore";
            this.txtGeneratedScore.Size = new System.Drawing.Size(100, 20);
            this.txtGeneratedScore.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Generated Score:";
            // 
            // txtSQLToExecute
            // 
            this.txtSQLToExecute.Location = new System.Drawing.Point(154, 187);
            this.txtSQLToExecute.Multiline = true;
            this.txtSQLToExecute.Name = "txtSQLToExecute";
            this.txtSQLToExecute.Size = new System.Drawing.Size(483, 74);
            this.txtSQLToExecute.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "SQL to EXECute:";
            // 
            // frmOneTimeScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 273);
            this.Controls.Add(this.txtSQLToExecute);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtGeneratedScore);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtGeneratedQuery);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnGenerateScore);
            this.Controls.Add(this.txtJobOpeningId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCandidateId);
            this.Controls.Add(this.label1);
            this.Name = "frmOneTimeScore";
            this.Text = "One Time C+JO TK Score";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCandidateId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtJobOpeningId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGenerateScore;
        private System.Windows.Forms.TextBox txtGeneratedQuery;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGeneratedScore;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSQLToExecute;
        private System.Windows.Forms.Label label5;

    }
}