﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace JMMBackOffice
{
    public partial class frmExportResume : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();
        const string DEFAULT_FOLDER = "c:\\MyResume";

        public frmExportResume()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = DEFAULT_FOLDER; // "c:\\MyResume"; // this presets the starting location      //folderBrowserDialog1.RootFolder = Environment.SpecialFolder.Desktop;
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtResumeFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnExportResume_Click(object sender, EventArgs e)
        {
            int candidate_Id = 0;

            ArrayList alCandidateIds = new ArrayList();
            alCandidateIds.AddRange(txtCandidateId.Text.Split(','));

            css = settings["JMM-Nov6Beta"]; //css = settings["JMM-Staging"];
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelCandidate_DocumentByType";

            conn.Open();

            foreach (var item in alCandidateIds)
            {
                try
                {
                    candidate_Id = int.Parse(item.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please enter a valid Candidate_Id");
                    return;
                }
                //MessageBox.Show(candidate_Id.ToString());
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
                cmd.Parameters.Add(new SqlParameter("Doc_Type_Code", 1));

                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();
                da.SelectCommand = cmd;
                da.Fill(dt);

                byte[] resume = (byte[])dt.Rows[0]["Document"];
                string dot_Extension = dt.Rows[0]["Dot_Extension"].ToString();

                File.WriteAllBytes(txtResumeFolder.Text + "\\" + candidate_Id.ToString() + "." + dot_Extension, resume);
            }

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void frmExportResume_Load(object sender, EventArgs e)
        {
            txtResumeFolder.Text = DEFAULT_FOLDER;
        }
    }
}
