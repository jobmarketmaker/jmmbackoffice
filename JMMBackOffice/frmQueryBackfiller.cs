﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Xml;

namespace JMMBackOffice
{
    public partial class frmQueryBackfiller : Form
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public frmQueryBackfiller()
        {
            InitializeComponent();
        }

        private static XmlDocument CreateSoapEnvelopeForQuery(string s)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://home.textkernel.nl/sourcebox/soap/documentProcessor""><soapenv:Header/><soapenv:Body><doc:processDocumentAdvanced><account>jobmarketmaker_cv</account><username>query</username><password>8226bG5N</password><clientSpecificArguments><key>?</key><value>?</value></clientSpecificArguments><fileName>resume.doc</fileName><fileContent>" + s + "</fileContent></doc:processDocumentAdvanced></soapenv:Body></soapenv:Envelope>");
            return soapEnvelop;
        }

        private void btnFillGrid_Click(object sender, EventArgs e)
        {
            BindGrid();
            btnGenerateQueries.Enabled = true;
        }

        private void BindGrid()
        {
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelQueryBackfill";
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);

            dgv.DataSource = dt;

            cmd = null;
            conn.Close();
            conn = null;
        }

        private void btnGenerateQueries_Click(object sender, EventArgs e)
        {
            int query_backfill_Id = 0;
            int candidate_Id = 0;
            string candidate_Query = "";

            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                try
                {
                    query_backfill_Id = int.Parse(dgvr.Cells[0].Value.ToString());
                    candidate_Id = int.Parse(dgvr.Cells[1].Value.ToString());

                    //-------------- first, get the query -------------------//
                    candidate_Query = GetCandidateQuery(candidate_Id).Replace("&", "&amp;");

                    dgvr.Cells[2].Value = candidate_Query;
                    Application.DoEvents();

                    SaveCandidateQuery(candidate_Id, candidate_Query);
                }
                catch (Exception ex)
                {
                    txtErrors.Text += ex.Message + "\n";
                    Application.DoEvents();
                }
            }
        }

        private string GetCandidateQuery(int candidate_Id)
        {
            string retVal = "";

            css = settings["JMM-Nov6Beta"];

            // retrieve the Candidate resume from the db
            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_SelCombosInPlay_CandidateResume"; // the CombosInPlay proc is okay here!!
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
            conn.Open();

            byte[] resume = (byte[])cmd.ExecuteScalar();
            cmd = null;
            conn.Close();
            conn = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://home.textkernel.nl:443/match/soap/documentProcessor"); // ("https://staging.textkernel.nl:443/match/soap/documentProcessor")
            string resumeBase64 = Convert.ToBase64String(resume);
            XmlDocument soapEnvelopXml = CreateSoapEnvelopeForQuery(resumeBase64);

            request.Method = "POST";
            request.ContentType = "text/xml";
            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }

            string temp = "";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                temp = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message);
            }

            XmlDocument justTheReturnNode = new XmlDocument();
            justTheReturnNode.LoadXml(temp);
            XmlNodeList xnl = justTheReturnNode.GetElementsByTagName("return");

            retVal = xnl.Item(0).InnerText;

            return retVal;
        }

        private void SaveCandidateQuery(int candidate_Id, string candidate_Query)
        {
            css = settings["JMM-Nov6Beta"];

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "up_UpdQueryBackfill_TKQuery";
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", candidate_Id));
            cmd.Parameters.Add(new SqlParameter("TK_Query", candidate_Query));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }
    }
}
