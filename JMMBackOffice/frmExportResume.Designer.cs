﻿namespace JMMBackOffice
{
    partial class frmExportResume
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtCandidateId = new System.Windows.Forms.TextBox();
            this.txtResumeFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnExportResume = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Candidate_Ids:";
            // 
            // txtCandidateId
            // 
            this.txtCandidateId.Location = new System.Drawing.Point(154, 12);
            this.txtCandidateId.Multiline = true;
            this.txtCandidateId.Name = "txtCandidateId";
            this.txtCandidateId.Size = new System.Drawing.Size(197, 116);
            this.txtCandidateId.TabIndex = 1;
            // 
            // txtResumeFolder
            // 
            this.txtResumeFolder.Location = new System.Drawing.Point(154, 141);
            this.txtResumeFolder.Name = "txtResumeFolder";
            this.txtResumeFolder.Size = new System.Drawing.Size(197, 20);
            this.txtResumeFolder.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Destination:";
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(357, 138);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFolder.TabIndex = 4;
            this.btnSelectFolder.Text = "Select folder";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnExportResume
            // 
            this.btnExportResume.Location = new System.Drawing.Point(154, 167);
            this.btnExportResume.Name = "btnExportResume";
            this.btnExportResume.Size = new System.Drawing.Size(230, 40);
            this.btnExportResume.TabIndex = 5;
            this.btnExportResume.Text = "Export Resumes";
            this.btnExportResume.UseVisualStyleBackColor = true;
            this.btnExportResume.Click += new System.EventHandler(this.btnExportResume_Click);
            // 
            // frmExportResume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 219);
            this.Controls.Add(this.btnExportResume);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResumeFolder);
            this.Controls.Add(this.txtCandidateId);
            this.Controls.Add(this.label1);
            this.Name = "frmExportResume";
            this.Text = "Export a Resume";
            this.Load += new System.EventHandler(this.frmExportResume_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCandidateId;
        private System.Windows.Forms.TextBox txtResumeFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnExportResume;
    }
}