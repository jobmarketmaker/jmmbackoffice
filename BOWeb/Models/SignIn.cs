﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOWeb.Models
{
    public class SignIn
    {
        [Required]
        [StringLength(100, MinimumLength = 1)]
        [DataType(DataType.Password)]
        [Display(Name = "Site Password")]
        public string Password { get; set; }
    }
}