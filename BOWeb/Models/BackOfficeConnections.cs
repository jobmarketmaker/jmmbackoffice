﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOWeb.Models
{
    public class BackOfficeConnections
    {
        [Display(Name = "TextKernel Connection")]
        public string TextKernelConnection { get; set; }

        [Display(Name = "JMM Database Connection")]
        public string JMMDBConnection { get; set; }
    }
}