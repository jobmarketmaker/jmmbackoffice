﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOWeb.Models
{
    public class Candidate
    {
        [Display(Name = "Candidate Id")]
        public int Candidate_Id { get; set; }
    
        [StringLength(50, ErrorMessage ="Candidate First Name cannot exceed 50 characters")]
        [Display(Name = "Candidate First Name")]
        public string Candidate_First_Name { get; set; }

        [StringLength(50, ErrorMessage = "Candidate Last Name cannot exceed 50 characters")]
        [Display(Name = "Candidate Last Name")]
        public string Candidate_Last_Name { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        [DataType(DataType.Password)]
        [Display(Name = "Candidate Password")]
        public string Password { get; set; }
    }
}