﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BOWeb.Classes;

// thank you http://stackoverflow.com/questions/12378445/asp-net-mvc-3-password-protect-view
namespace BOWeb.Controllers
{

    public class ToolsController : Controller
    {
        [SimpleMembership]
        public ActionResult Index()
        {
            return View();
        }

        [SimpleMembership]
        public ActionResult CheckTKCurrency()
        {
            return View();
        }

        [SimpleMembership]
        public ActionResult CreateOneTimeScore()
        {
            return View();
        }

        [SimpleMembership]
        public ActionResult ExportResumes()
        {
            return View();
        }


        //[SimpleMembership]
        [HttpGet]
        public ActionResult SignIn()
        {
            return View();
        }

        //[SimpleMembership]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SignIn(BOWeb.Models.SignIn SignIn)
        {
            if (ModelState.IsValid)
            {
                HttpContext.Session["myApp-Authentication"] = SignIn.Password;
                //string redirectOnSuccess = HttpContext.Request.Url.AbsolutePath;
                if (SignIn.Password == "bingobangobongo")
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


        [SimpleMembership]
        [HttpGet]
        public ActionResult ResetCandidatePassword()
        {
            return View();
        }

        [SimpleMembership]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ResetCandidatePassword(BOWeb.Models.Candidate Candidate)
        {
            if (ModelState.IsValid)
            {
                Utilities u = new Utilities();
                u.UpdateCandidatePassword(Candidate); //(Candidate.Candidate_Id, Candidate.Password);
                ViewBag.StatusMessage = "Password reset successfully!";
            }
            else
            {
                ViewBag.StatusMessage = "There was a problem with your input data.";
            }
            return View();
        }

        [SimpleMembership]
        [HttpGet]
        public ActionResult ChangeConnections()
        {
            return View();
        }

        [SimpleMembership]
        public ActionResult ChangeConnections(BOWeb.Models.BackOfficeConnections BOConnections)
        {
            Utilities u = new Utilities();
            u.SaveBackOfficeConnections(BOConnections);
            return View();
        }

        [SimpleMembership]
        public ActionResult UploadJobBoardImage()
        {
            return View();
        }
    }
}