﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BOWeb.Startup))]
namespace BOWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
