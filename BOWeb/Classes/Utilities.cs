﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BOWeb.Models;

namespace BOWeb.Classes
{
    public class Utilities
    {
        ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
        ConnectionStringSettings css = new ConnectionStringSettings();

        public Utilities()
        {
            if (HttpContext.Current.Session["JMM"] == null)
            {
                css = settings["JMM-Dev"];
            }
            else
            {
                switch (HttpContext.Current.Session["JMM"].ToString())
                {
                    case "dev":
                        css = settings["JMM-Dev"];
                        break;
                    case "staging":
                        css = settings["JMM-Staging"];
                        break;
                    case "sandbox":
                        css = settings["JMM-Sandbox"];
                        break;
                    case "prod":
                        css = settings["JMM-Nov6Beta"];
                        break;
                }
            }
        }

        public void UpdateCandidatePassword (Candidate Candidate) // (int Candidate_Id, string Password)
        {
            // .Net implementation of BCrypt
            // thank you http://www.codeproject.com/Articles/475262/UseplusBCryptplustoplusHashplusYourplusPasswords and http://derekslager.com/blog/attachment.ashx/bcrypt-dotnet-strong-password-hashing-for-dotnet-and-mono/BCrypt.cs
            string mySalt = BCrypt.GenerateSalt(); //"bacon";
            string myHash = BCrypt.HashPassword(Candidate.Password, mySalt);

            SqlConnection conn = new SqlConnection(css.ConnectionString);
            SqlCommand cmd = new SqlCommand("up_UpdCandidatePassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("Candidate_Id", Candidate.Candidate_Id));
            cmd.Parameters.Add(new SqlParameter("Password", myHash)); // Password));

            conn.Open();
            cmd.ExecuteNonQuery();

            cmd = null;
            conn.Close();
            conn = null;
        }

        public void SaveBackOfficeConnections (BackOfficeConnections BOConnections)
        {
            HttpContext.Current.Session["TK"] = BOConnections.TextKernelConnection;
            HttpContext.Current.Session["JMM"] = BOConnections.JMMDBConnection;
            //Session["blah"] = "a";
            string s = "";
        }
    }
}